#include "Macros.h"
#include "Log.h"

int main(int argc, char* argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    for (int i = 0; i < 100; ++i)
    {
        Log("Test " + std::to_string(i)) << "This is a test";
    }

    return 0;
}
