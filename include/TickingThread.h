#ifndef TickingThread_h
#define TickingThread_h

#include "Thread.h"
#include "SyncedQueue.h"

///////////////////////////////////////////////////////////////////////////////
class Tick
{
public:
    Tick() : m_nType(0) {}
    Tick(const int nType) : m_nType(nType) {}
    virtual ~Tick() {}
    int getType() const { return m_nType; }
    void setType(const int nType) { m_nType = nType; }

    static void deleteTick(Tick* pTick) { if (pTick) { delete pTick; } };

protected:
    int m_nType;
};

///////////////////////////////////////////////////////////////////////////////
class TickingThread : public Thread
{
public:
    TickingThread();
    virtual ~TickingThread();

    virtual void quit() override;
    void post(Tick*const pTickEvent);

protected:
    virtual void run() override;
    virtual void tick(Tick*const pTick) = 0;

private:
    DISABLE_COPY(TickingThread)

    SyncedQueue<Tick*> m_q;
};

#endif
