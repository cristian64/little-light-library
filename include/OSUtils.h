#ifndef OSUtils_h
#define OSUtils_h

#include <list>
#include <string>

namespace OSUtils
{

std::list<std::string> getFilesInDirectory(const std::string &strDirPath);
std::string cleanPath(const std::string &strPath);
bool isAbsolutePath(const std::string &strPath);
char getNativeSeparator();

}

#endif
