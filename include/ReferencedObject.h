#ifndef ReferencedObject_h
#define ReferencedObject_h

#include "Macros.h"

#include <atomic>

///////////////////////////////////////////////////////////////////////////////
class ReferencedObject
{
public:
    ReferencedObject();
    virtual ~ReferencedObject();
    int getRefCount() const;
    void addRef();
    void delRef();

protected:
    virtual void onDereferenced() = 0;

private:
    DISABLE_COPY(ReferencedObject)

    std::atomic_int m_nReferenceCount;
};

///////////////////////////////////////////////////////////////////////////////
template <typename T>
class ReferencedObjectContainer final
{
public:
    ReferencedObjectContainer() : m_pObject(nullptr)
    {
    }

    ReferencedObjectContainer(T*const pObject) : m_pObject(pObject)
    {
        if (pObject) {
            pObject->addRef();
        }
    }

    ReferencedObjectContainer(const ReferencedObjectContainer& other) : m_pObject(other.m_pObject)
    {
        if (m_pObject) {
            m_pObject->addRef();
        }
    }

    ReferencedObjectContainer(ReferencedObjectContainer&& other) : m_pObject(other.m_pObject)
    {
        other.m_pObject = nullptr;
    }

    ~ReferencedObjectContainer()
    {
        if (m_pObject) {
            m_pObject->delRef();
        }
    }

    ReferencedObjectContainer& operator=(T*const pObject)
    {
        if (m_pObject) {
            m_pObject->delRef();
        }
        m_pObject = pObject;
        if (m_pObject) {
            m_pObject->addRef();
        }
        return *this;
    }

    ReferencedObjectContainer& operator=(const ReferencedObjectContainer& other)
    {
        if (this != &other)
        {
            if (m_pObject) {
                m_pObject->delRef();
            }
            m_pObject = other.m_pObject;
            if (m_pObject) {
                m_pObject->addRef();
            }
        }
        return *this;
    }

    ReferencedObjectContainer& operator=(ReferencedObjectContainer&& other)
    {
        if (this != &other)
        {
            if (m_pObject) {
                m_pObject->delRef();
            }
            m_pObject = other.m_pObject;
            other.m_pObject = nullptr;
        }
        return *this;
    }

    operator bool() const { return m_pObject != nullptr; }
    T* operator->() { return m_pObject; }
    const T* operator->() const { return m_pObject; }
    T& operator*() { return *m_pObject; }
    const T& operator*() const { return *m_pObject; }
    T* get() { return m_pObject; }
    const T* get() const { return m_pObject; }

private:
    T* m_pObject;

};

#endif
