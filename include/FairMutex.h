#ifndef FairMutex_h
#define FairMutex_h

#include "Macros.h"

#include <mutex>
#include <condition_variable>
#include <set>

class FairMutex final
{
public:
    FairMutex();
    void lock();
    void unlock();
    bool tryLock(const unsigned int nTimeoutMs = 0);

private:
    DISABLE_COPY(FairMutex)

    std::mutex m_mutex;
    std::condition_variable m_conditionVariable;
    int m_nTicketGenerator;
    int m_nNextTicket;
    std::set<int> setTimedoutTickets;
};


class FairMutexLocker
{
public:
    FairMutexLocker(FairMutex& mutex);
    ~FairMutexLocker();

private:
    DISABLE_COPY(FairMutexLocker)

    FairMutex& m_mutex;
};

#endif
