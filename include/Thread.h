#ifndef Thread_h
#define Thread_h

#include "Macros.h"
#include "Semaphore.h"

#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

class Thread
{
public:
    Thread();
    virtual ~Thread();

    virtual bool start();
    virtual void quit();

    bool isRunning() const;
    bool isCancelled() const;
    bool isFinished() const;
    bool wait(const unsigned int nTimeoutMs = ~0) const;
    bool waitUntilRunning(const unsigned int nTimeoutMs = ~0) const;
    bool terminate();

    bool checkIfSuccessfullyStarted(const unsigned int nTimeoutMs/* = 0*/) const;
    void releaseStart(const bool bSuccess);

    static void ssleep(const unsigned int nSleepS);
    static void msleep(const unsigned int nSleepMs);
    static void usleep(const unsigned int nSleepUs);
    static void nsleep(const unsigned int nSleepNs);
    static void yield();

    using Id = uint64_t;
    Id getId() const;
    void* getNativeHandle() const;
    bool setName(const std::string& strName);
    bool getName(std::string& strName) const;
    std::string getName() const;

    static bool setThreadName(const Thread::Id id, const std::string& strName);
    static bool getThreadName(const Thread::Id id, std::string& strName);
    static std::string getThreadName(const Thread::Id id);
    static Id getCurrentThreadId();
    static bool setCurrentThreadName(const std::string& strName);
    static bool getCurrentThreadName(std::string& strName);
    static std::string getCurrentThreadName();

protected:
    virtual void run() = 0;

private:
    void _run_();

    DISABLE_COPY(Thread)

    std::mutex m_mutexThread;
    std::unique_ptr<std::thread> m_apThread;

    std::atomic<Id> m_id;
    std::atomic<void*> m_handle;
    mutable std::timed_mutex m_mutexRunning;
    std::atomic_bool m_bCancelled;
    std::atomic_bool m_bFinished;

    mutable std::mutex m_mutexWaitUntilRunning;
    mutable std::condition_variable m_conditionVariableRunning;

    mutable Semaphore m_semStart;
    std::atomic_bool m_bStartedSuccessfully;
};

#endif
