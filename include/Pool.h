#ifndef Pool_h
#define Pool_h

#include "Macros.h"
#include "ReferencedObject.h"
#include "PoolContent.h"

#include <mutex>
#include <condition_variable>
#include <unordered_set>

class Pool : public ReferencedObject
{
    friend class PoolContent;

public:
    Pool();
    virtual ~Pool();

    bool cancel(const unsigned int nTimeoutMs = 0);

    std::string getPoolName() const;
    void setPoolName(const std::string& strPoolName);

    void getPoolStatus(size_t& nInside, size_t& nOutside) const;
    void addContent(PoolContent*const pContent);
    bool getContent(PoolContentContainer& container, const unsigned int nTimeoutMs = 0);

    template <typename T>
    bool getContent(T& container, const unsigned int nTimeoutMs = 0)
    {
        PoolContentContainer c;
        if (getContent(c, nTimeoutMs)) {
            container = dynamic_cast<decltype(container.operator->())>(c.operator->());
            return true;
        }
        return false;
    }

protected:
    void onDereferenced() override;

private:
    DISABLE_COPY(Pool)

    void returnContent(PoolContent*const pContent);

    mutable std::mutex m_mutex;
    mutable std::condition_variable m_conditionVariable;

    std::unordered_set<PoolContent*> m_mapInside;
    std::unordered_set<PoolContent*> m_mapOutside;

    std::string m_strPoolName;
    bool m_bCancelled;
};

typedef ReferencedObjectContainer<Pool> PoolContainer;

#endif
