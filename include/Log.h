#ifndef Log_h
#define Log_h

#include <mutex>
#include <list>
#include <string>

class Log
{
public:
    Log();
    Log(const int nLevel);
    Log(const int nLevel, const std::string& strSystem);
    Log(const std::string& strSystem);
    ~Log();

    Log& operator<<(const char*const str);
    Log& operator<<(const std::string& str);
    Log& operator<<(const bool b);
    Log& operator<<(const char c);

#define OPE(x) inline Log& operator<<(const x n) { m_lstParts.push_back(std::to_string(n)); return *this; }
    OPE(short);
    OPE(unsigned short);
    OPE(int);
    OPE(long);
    OPE(long long);
    OPE(unsigned);
    OPE(unsigned long);
    OPE(unsigned long long);
    OPE(float);
    OPE(double);
    OPE(long double);
#undef OPE

private:
    static std::mutex s_mutex;
    const int m_nLevel;
    const std::string m_strSystem;
    std::list<std::string> m_lstParts;
};

#endif
