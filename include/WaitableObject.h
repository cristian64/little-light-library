#ifndef WaitableObject_h
#define WaitableObject_h

#include "Macros.h"

#include <mutex>
#include <condition_variable>

class WaitableObject
{
public:
    WaitableObject();
    virtual ~WaitableObject();

    void incCount(const unsigned int nCount = 1);
    void decCount(const unsigned int nCount = 1);
    bool waitForObject(const unsigned int nTimeoutMs = ~0) const;
    void flush();

private:
    DISABLE_COPY(WaitableObject)

    mutable std::mutex m_mutex;
    mutable std::condition_variable m_conditionVariable;
    unsigned int m_nCount;
};

#endif
