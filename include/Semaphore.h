#ifndef Semaphore_h
#define Semaphore_h

#include "Macros.h"

#include <mutex>
#include <condition_variable>

class Semaphore final
{
public:
    Semaphore(const unsigned int nCount = 0);

    void release(const unsigned int nCount = 1);
    void acquire(const unsigned int nCount = 1);
    bool tryAcquire(const unsigned int nCount = 1, const unsigned int nTimeoutMs = 0);

private:
    DISABLE_COPY(Semaphore)

    std::mutex m_mutex;
    std::condition_variable m_conditionVariable;
    unsigned int m_nCount;
};

#endif
