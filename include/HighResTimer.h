#ifndef HighResTimer_h
#define HighResTimer_h

#include <chrono>
#include <cstdint>

class HighResTimer final
{
public:
    HighResTimer();
    void restart();
    uint64_t periods() const;
    uint64_t periodms() const;
    uint64_t periodus() const;
    uint64_t periodns() const;

private:
    std::chrono::time_point<std::chrono::steady_clock> m_init;
};

#endif
