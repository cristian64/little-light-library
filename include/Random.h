#ifndef Random_h
#define Random_h

class Random final
{
public:
    Random(const int nSeed = 0);
    void reset(const int nSeed = 0);
    int rand();

private:
    int m_nSeed;
};

#endif
