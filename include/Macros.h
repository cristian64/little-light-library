#ifndef Macros_h
#define Macros_h

#include <cassert>

#define DISABLE_COPY(Class) Class(const Class&) = delete; Class& operator=(const Class&) = delete;
#define ASSERT_HRESULT(x) (FAILED(x) ? (assert(0), false) : true)
#define UNUSED(x) ((void)(x))

#endif
