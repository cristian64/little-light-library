#ifndef SyncedQueue_h
#define SyncedQueue_h

#include "Macros.h"

#include <mutex>
#include <condition_variable>
#include <list>

template<typename T>
class SyncedQueue
{
public:
    SyncedQueue();
    SyncedQueue(const std::function<void(const T&)> fnDelete);

    bool push(const T& item);
    bool waitForItem(T& item);
    bool waitForItem(T& item, const unsigned int nTimeoutMs);

    void cancel();
    bool isCancelled() const;

    unsigned long getMaxCapacity() const;
    void setMaxCapacity(const unsigned long nMaxCapacity);

private:
    DISABLE_COPY(SyncedQueue)

    bool m_bCancelled;
    mutable std::mutex m_mutex;
    std::condition_variable m_conditionVariable;
    std::list<T> m_q;
    const std::function<void(const T&)> m_fnDelete;
    unsigned long m_nMaxCapacity;
};

///////////////////////////////////////////////////////////////////////////////
template<typename T>
SyncedQueue<T>::SyncedQueue() : m_bCancelled(false), m_nMaxCapacity(~0)
{
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
SyncedQueue<T>::SyncedQueue(const std::function<void(const T&)> fnDelete) : m_bCancelled(false), m_fnDelete(fnDelete), m_nMaxCapacity(~0)
{
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool SyncedQueue<T>::push(const T& item)
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        if (m_bCancelled) {
            if (m_fnDelete) {
                m_fnDelete(item);
            }
            return false;
        }

        if (m_q.size() >= m_nMaxCapacity) {
            if (m_fnDelete) {
                m_fnDelete(m_q.front());
            }
            m_q.pop_front();
        }
        m_q.push_back(item);
    }
    m_conditionVariable.notify_one();
    return true;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool SyncedQueue<T>::waitForItem(T& item)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    while (m_q.size() == 0)
    {
        if (m_bCancelled)
        {
            item = T();
            return false;
        }

        m_conditionVariable.wait(lock);
    }
    item = m_q.front();
    m_q.pop_front();
    return true;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool SyncedQueue<T>::waitForItem(T& item, const unsigned int nTimeoutMs)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    unsigned long long int nTimeLeftMs = nTimeoutMs;
    while (m_q.size() == 0)
    {
        if (nTimeLeftMs == 0 || m_bCancelled)
        {
            item = T();
            return false;
        }

        const auto tNow = std::chrono::steady_clock::now();
        const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
        if (bTimeout)
        {
            item = T();
            return false;
        }
        const auto tThen = std::chrono::steady_clock::now();
        const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
        const unsigned long long int nElapsedMs = tElapsed.count();
        nTimeLeftMs -= nElapsedMs;
    }

    item = m_q.front();
    m_q.pop_front();

    return true;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
void SyncedQueue<T>::cancel()
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);

        m_bCancelled = true;

        while (m_q.size())
        {
            if (m_fnDelete) {
                m_fnDelete(m_q.front());
            }
            m_q.pop_front();
        }
    }

    m_conditionVariable.notify_all();
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool SyncedQueue<T>::isCancelled() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_bCancelled;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
unsigned long SyncedQueue<T>::getMaxCapacity() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_nMaxCapacity;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
void SyncedQueue<T>::setMaxCapacity(const unsigned long nMaxCapacity)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_nMaxCapacity = nMaxCapacity;
    while (m_q.size() > m_nMaxCapacity)
    {
        if (m_fnDelete) {
            m_fnDelete(m_q.front());
        }
        m_q.pop_front();
    }
}

#endif
