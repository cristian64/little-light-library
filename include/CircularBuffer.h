#ifndef CircularBuffer_h
#define CircularBuffer_h

#include "Macros.h"

#include <cstring>
#include <cstdint>
#include <algorithm>

template<typename T>
class CircularBuffer final
{

public:

    class const_iterator
    {
    public:
        const_iterator(const CircularBuffer<T>& cb, const bool bBegin) :
            m_cb(cb),
            m_nIndex(bBegin && m_cb.m_nCapacity ? 0 : ~uint64_t(0)) {}
        const_iterator(const const_iterator& it) : m_cb(it.m_cb), m_nIndex(it.m_nIndex) {}
        const_iterator operator++(int) { const_iterator it(*this); ++(*this); return it; }
        const_iterator& operator++()
        {
            if (isEnd()) { return *this; }
            if (isLast()) { m_nIndex = ~uint64_t(0); return *this; }
            m_nIndex = (m_nIndex + 1) % m_cb.m_nCount;
            return *this;
        }
        const_iterator operator--(int) { const_iterator it(*this); --(*this); return it; }
        const_iterator& operator--()
        {
            if (isFirst()) { return *this; }
            if (isEnd())
            {
                m_nIndex = m_cb.m_nCount ? m_cb.m_nCount - 1 : ~uint64_t(0);
                return *this;
            }
            m_nIndex = m_cb.m_nCount ? m_nIndex - 1 : ~uint64_t(0);
            return *this;
        }
        bool operator!=(const const_iterator& it) const { return m_nIndex != it.m_nIndex; }
        bool operator==(const const_iterator& it) const { return m_nIndex == it.m_nIndex; }
        const T& operator*() const { return m_cb[m_nIndex]; }

    protected:
        inline bool isFirst() const { return m_nIndex == 0; }
        inline bool isLast() const { return (m_nIndex + 1) == m_cb.m_nCount; }
        inline bool isEnd() const { return m_nIndex == ~uint64_t(0); }

        const CircularBuffer<T>& m_cb;
        uint64_t m_nIndex;

    };

    class iterator : public const_iterator
    {
    public:
        iterator(CircularBuffer<T>& cb, const bool bBegin) : const_iterator(cb, bBegin) {}
        T& operator*() { return const_cast<CircularBuffer<T>*>(&this->m_cb)->operator[](this->m_nIndex); }
    };

    CircularBuffer();
    CircularBuffer(const uint64_t nCapacity);
    CircularBuffer(const std::initializer_list<T>& list);
    CircularBuffer(const CircularBuffer& cb);
    CircularBuffer(CircularBuffer&& cb);
    ~CircularBuffer();

    CircularBuffer& operator=(const CircularBuffer& cb);
    CircularBuffer& operator=(CircularBuffer&& cb);

    T& operator[](const uint64_t nIndex);
    const T& operator[](const uint64_t nIndex) const;

    uint64_t getCapacity() const;
    uint64_t getCount() const;
    uint64_t getFreeSpace() const;
    bool isFull() const;
    bool isEmpty() const;
    const T* getRawBuffer() const;
    T* getRawBuffer();

    void reset(const uint64_t nCapacity);
    void resize(const uint64_t nCapacity);
    void clear();

    bool push(const T& item);
    uint64_t push(const T*const pBuffer, const uint64_t nCount);
    bool pop(T& item);
    uint64_t pop(T*const pBuffer, const uint64_t nCount);
    bool peek(T& item) const;
    uint64_t peek(T*const pBuffer, const uint64_t nCount) const;
    uint64_t drop(const uint64_t nCount);
    uint64_t stuff(const uint64_t nCount);

    iterator begin();
    const_iterator begin() const;
    const_iterator cbegin() const;
    iterator end();
    const_iterator end() const;
    const_iterator cend() const;

private:
    uint64_t m_nCapacity;
    uint64_t m_nNext;
    uint64_t m_nCount;
    T* m_pBuffer;
};

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>::CircularBuffer() :
    m_nCapacity(0),
    m_nNext(0),
    m_nCount(0),
    m_pBuffer(nullptr)
{
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>::CircularBuffer(const uint64_t nCapacity) :
    m_nCapacity(nCapacity),
    m_nNext(0),
    m_nCount(0),
    m_pBuffer(nCapacity ? (new T[nCapacity]) : nullptr)
{
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>::CircularBuffer(const std::initializer_list<T>& list) :
    m_nCapacity(list.size()),
    m_nNext(0),
    m_nCount(m_nCapacity),
    m_pBuffer(m_nCapacity ? (new T[m_nCapacity]) : nullptr)
{
    uint64_t i = 0;
    for (const T& item : list) {
        m_pBuffer[i++] = item;
    }
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>::CircularBuffer(const CircularBuffer& cb) :
    m_nCapacity(cb.m_nCapacity),
    m_nNext(cb.m_nNext),
    m_nCount(cb.m_nCount),
    m_pBuffer(m_nCapacity ? (new T[m_nCapacity]) : nullptr)
{
    if (m_nCapacity)
    {
        memcpy(m_pBuffer, cb.m_pBuffer, sizeof(T) * m_nCapacity);
    }
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>::CircularBuffer(CircularBuffer&& cb) :
    m_nCapacity(cb.m_nCapacity),
    m_nNext(cb.m_nNext),
    m_nCount(cb.m_nCount),
    m_pBuffer(cb.m_pBuffer)
{
    cb.m_pBuffer = nullptr;
    cb.m_nCapacity = cb.m_nNext = cb.m_nCount = 0;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>::~CircularBuffer()
{
    if (m_pBuffer) { delete [] m_pBuffer; m_pBuffer = nullptr; }
    m_nCapacity = m_nNext = m_nCount = 0;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>& CircularBuffer<T>::operator=(const CircularBuffer& cb)
{
    if (this == &cb) { return *this; }
    m_nCapacity = cb.m_nCapacity;
    m_nNext = cb.m_nNext;
    m_nCount = cb.m_nCount;
    if (m_pBuffer) { delete [] m_pBuffer; m_pBuffer = nullptr; }
    if (m_nCapacity)
    {
        m_pBuffer = new T[m_nCapacity];
        memcpy(m_pBuffer, cb.m_pBuffer, sizeof(T) * m_nCapacity);
    }
    return *this;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
CircularBuffer<T>& CircularBuffer<T>::operator=(CircularBuffer&& cb)
{
    if (this == &cb) { return *this; }
    m_nCapacity = cb.m_nCapacity;
    m_nNext = cb.m_nNext;
    m_nCount = cb.m_nCount;
    if (m_pBuffer) { delete [] m_pBuffer; }
    m_pBuffer = cb.m_pBuffer;

    cb.m_nCapacity = cb.m_nNext = cb.m_nCount = 0;
    cb.m_pBuffer = nullptr;
    return *this;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
T& CircularBuffer<T>::operator[](const uint64_t nIndex)
{
    const uint64_t nTail = (m_nCapacity + m_nNext - m_nCount + nIndex) % m_nCapacity;
    return m_pBuffer[nTail];
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
const T& CircularBuffer<T>::operator[](const uint64_t nIndex) const
{
    const uint64_t nTail = (m_nCapacity + m_nNext - m_nCount + nIndex) % m_nCapacity;
    return m_pBuffer[nTail];
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::getCapacity() const
{
    return m_nCapacity;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::getCount() const
{
    return m_nCount;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::getFreeSpace() const
{
    return m_nCapacity - m_nCount;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool CircularBuffer<T>::isFull() const
{
    return m_nCapacity == m_nCount;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool CircularBuffer<T>::isEmpty() const
{
    return 0 == m_nCount;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
const T* CircularBuffer<T>::getRawBuffer() const
{
    return m_pBuffer;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
T* CircularBuffer<T>::getRawBuffer()
{
    return m_pBuffer;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
void CircularBuffer<T>::reset(const uint64_t nCapacity)
{
    if (m_nCapacity != nCapacity)
    {
        m_nCapacity = nCapacity;
        if (m_pBuffer) { delete [] m_pBuffer; m_pBuffer = nullptr; }
        if (nCapacity) { m_pBuffer = new T[nCapacity]; }
    }
    m_nNext = m_nCount = 0;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
void CircularBuffer<T>::resize(const uint64_t nCapacity)
{
    if (m_nCapacity == nCapacity) { return; }

    CircularBuffer cb(nCapacity);
    cb.stuff(pop(cb.getRawBuffer(), nCapacity));
    this->operator=(cb);
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
void CircularBuffer<T>::clear()
{
    m_nNext = m_nCount = 0;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool CircularBuffer<T>::push(const T& item)
{
    if (m_nCapacity <= m_nCount) { return false; }

    m_pBuffer[m_nNext] = item;
    m_nNext = (m_nNext + 1) % m_nCapacity;
    ++m_nCount;
    return true;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::push(const T*const pBuffer, const uint64_t nCount)
{
    const uint64_t nMaxToWrite = std::min(m_nCapacity - m_nCount, nCount);
    if (!pBuffer || !nMaxToWrite) { return 0; }

    if (m_nNext + nMaxToWrite < m_nCapacity)
    {
        // Write in one step.
        memcpy(&m_pBuffer[m_nNext], pBuffer, sizeof(T) * nMaxToWrite);
    }
    else
    {
        // Write in two steps.
        const uint64_t nFirstStepCount = m_nCapacity - m_nNext;
        const uint64_t nSecondStepCount = nMaxToWrite - nFirstStepCount;
        memcpy(&m_pBuffer[m_nNext], pBuffer, sizeof(T) * nFirstStepCount);
        memcpy(m_pBuffer, &pBuffer[nFirstStepCount], sizeof(T) * nSecondStepCount);
    }

    m_nNext = (m_nNext + nMaxToWrite) % m_nCapacity;
    m_nCount += nMaxToWrite;
    return nMaxToWrite;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool CircularBuffer<T>::pop(T& item)
{
    if (0 == m_nCount) { return false; }

    const uint64_t nTail = (m_nCapacity + m_nNext - m_nCount) % m_nCapacity;
    item = m_pBuffer[nTail];
    --m_nCount;
    return true;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::pop(T*const pBuffer, const uint64_t nCount)
{
    const uint64_t nMaxToRead = std::min(m_nCount, nCount);
    if (!pBuffer || !nMaxToRead) { return 0; }

    const uint64_t nTail = (m_nCapacity + m_nNext - m_nCount) % m_nCapacity;
    if (nTail + nMaxToRead < m_nCapacity)
    {
        // Write in one step.
        memcpy(pBuffer, &m_pBuffer[nTail], sizeof(T) * nMaxToRead);
    }
    else
    {
        // Write in two steps.
        const uint64_t nFirstStepCount = m_nCapacity - nTail;
        const uint64_t nSecondStepCount = nMaxToRead - nFirstStepCount;
        memcpy(pBuffer, &m_pBuffer[nTail], sizeof(T) * nFirstStepCount);
        memcpy(&pBuffer[nFirstStepCount], m_pBuffer, sizeof(T) * nSecondStepCount);
    }

    m_nCount -= nMaxToRead;
    return nMaxToRead;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool CircularBuffer<T>::peek(T& item) const
{
    if (0 == m_nCount) { return false; }

    const uint64_t nTail = (m_nCapacity + m_nNext - m_nCount) % m_nCapacity;
    item = m_pBuffer[nTail];
    return true;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::peek(T*const pBuffer, const uint64_t nCount) const
{
    const uint64_t nMaxToRead = std::min(m_nCount, nCount);
    if (!pBuffer || !nMaxToRead) { return 0; }

    const uint64_t nTail = (m_nCapacity + m_nNext - m_nCount) % m_nCapacity;
    if (nTail + nMaxToRead < m_nCapacity)
    {
        // Write in one step.
        memcpy(pBuffer, &m_pBuffer[nTail], sizeof(T) * nMaxToRead);
    }
    else
    {
        // Write in two steps.
        const uint64_t nFirstStepCount = m_nCapacity - nTail;
        const uint64_t nSecondStepCount = nMaxToRead - nFirstStepCount;
        memcpy(pBuffer, &m_pBuffer[nTail], sizeof(T) * nFirstStepCount);
        memcpy(&pBuffer[nFirstStepCount], m_pBuffer, sizeof(T) * nSecondStepCount);
    }

    return nMaxToRead;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::drop(const uint64_t nCount)
{
    const uint64_t nMaxToRead = std::min(m_nCount, nCount);
    m_nCount -= nMaxToRead;
    return nMaxToRead;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
uint64_t CircularBuffer<T>::stuff(const uint64_t nCount)
{
    const uint64_t nMaxToWrite = std::min(m_nCapacity - m_nCount, nCount);
    if (!nMaxToWrite) { return 0; }

    m_nNext = (m_nNext + nMaxToWrite) % m_nCapacity;
    m_nCount += nMaxToWrite;
    return nMaxToWrite;
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
typename CircularBuffer<T>::iterator CircularBuffer<T>::begin()
{
    return iterator(*this, true);
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
typename CircularBuffer<T>::const_iterator CircularBuffer<T>::begin() const
{
    return const_iterator(*this, true);
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
typename CircularBuffer<T>::const_iterator CircularBuffer<T>::cbegin() const
{
    return const_iterator(*this, true);
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
typename CircularBuffer<T>::iterator CircularBuffer<T>::end()
{
    return iterator(*this, false);
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
typename CircularBuffer<T>::const_iterator CircularBuffer<T>::end() const
{
    return const_iterator(*this, false);
}

///////////////////////////////////////////////////////////////////////////////
template<typename T>
typename CircularBuffer<T>::const_iterator CircularBuffer<T>::cend() const
{
    return const_iterator(*this, false);
}


#endif