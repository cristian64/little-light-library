#ifndef ReadWriteMutex_h
#define ReadWriteMutex_h

#include "Macros.h"

#include <mutex>
#include <condition_variable>

class ReadWriteMutex final
{
public:
    ReadWriteMutex();
    void lockForRead();
    void lockForWrite();
    void unlockForRead();
    void unlockForWrite();
    bool tryLockForRead(const unsigned int nTimeoutMs = 0);
    bool tryLockForWrite(const unsigned int nTimeoutMs = 0);

private:
    DISABLE_COPY(ReadWriteMutex)

    std::mutex m_mutex;
    std::condition_variable m_conditionVariable;
    unsigned int m_nReaders;
    unsigned int m_nWriters;
};


class ReadMutexLocker final
{
public:
    ReadMutexLocker(ReadWriteMutex& mutex);
    ~ReadMutexLocker();

private:
    DISABLE_COPY(ReadMutexLocker)

    ReadWriteMutex& m_mutex;
};


class WriteMutexLocker final
{
public:
    WriteMutexLocker(ReadWriteMutex& mutex);
    ~WriteMutexLocker();

private:
    DISABLE_COPY(WriteMutexLocker)

    ReadWriteMutex& m_mutex;
};

#endif
