#ifndef Mutex_h
#define Mutex_h

#include "Macros.h"

#include <mutex>

class Mutex final
{
public:
    Mutex();
    void lock();
    void unlock();
    bool tryLock(const unsigned int nTimeoutMs = 0);

private:
    DISABLE_COPY(Mutex)

    std::timed_mutex m_mutex;
};


class MutexLocker
{
public:
    MutexLocker(Mutex& mutex);
    ~MutexLocker();

private:
    DISABLE_COPY(MutexLocker)

    Mutex& m_mutex;
};

#endif
