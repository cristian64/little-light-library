#ifndef PoolContent_h
#define PoolContent_h


#include "ReferencedObject.h"
#include "WaitableObject.h"

#include <mutex>

class Pool;
typedef ReferencedObjectContainer<Pool> PoolContainer;

class PoolContent : public ReferencedObject
{
public:
    PoolContent();
    virtual ~PoolContent();
    void setPool(const PoolContainer& container);

protected:
    void onDereferenced() override;

private:
    DISABLE_COPY(PoolContent)

    std::mutex m_mutex;
    PoolContainer m_contentPoolContainer;
};

typedef ReferencedObjectContainer<PoolContent> PoolContentContainer;

#endif
