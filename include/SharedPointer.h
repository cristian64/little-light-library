#ifndef SharedPointer_h
#define SharedPointer_h

#include "Macros.h"

#include <mutex>

template<typename T>
class WeakPointer;

///////////////////////////////////////////////////////////////////////////////
template<typename T>
class SharedPointer final
{
    struct SharedPointerInternal final
    {
        mutable std::mutex m_mutex;
        T* m_pData;
        int m_nRefCount;
        int m_nOwnerCount;

        SharedPointerInternal(T*const pData) : m_pData(pData), m_nRefCount(1), m_nOwnerCount(1)
        {
        }

        ~SharedPointerInternal()
        {
        }

        int getRefCount() const
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            return m_nRefCount;
        }

        int addRef()
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            return ++m_nRefCount;
        }

        int delRef()
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            const int nRefCount = (m_nRefCount > 0) * (m_nRefCount - 1);
            m_nRefCount = nRefCount;
            if (!nRefCount) { delete this; }
            return nRefCount;
        }

        int getOwnerCount() const
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            return m_nOwnerCount;
        }

        int addOwner()
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            if (m_nOwnerCount)
            {
                return ++m_nOwnerCount;
            }
            return 0;
        }

        int delOwner()
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            const int nOwnerCount = (m_nOwnerCount > 0) * (m_nOwnerCount - 1);
            m_nOwnerCount = nOwnerCount;
            if (!nOwnerCount && m_pData)
            {
                delete m_pData;
                m_pData = nullptr;
            }
            return nOwnerCount;
        }

        DISABLE_COPY(SharedPointerInternal)
    };

    T* m_pData;
    SharedPointerInternal* m_pInternal;

public:
    SharedPointer() : m_pData(nullptr), m_pInternal(nullptr)
    {
    }

    SharedPointer(T*const pData) : m_pData(pData), m_pInternal(pData ? new SharedPointerInternal(pData) : nullptr)
    {
    }

    SharedPointer(const SharedPointer& sp) : m_pData(sp.m_pData), m_pInternal(sp.m_pInternal)
    {
        if (m_pInternal) {
            m_pInternal->addRef();
            m_pInternal->addOwner();
        }
    }

    SharedPointer(SharedPointer&& sp) : m_pData(sp.m_pData), m_pInternal(sp.m_pInternal)
    {
        sp.m_pData = nullptr;
        sp.m_pInternal = nullptr;
    }

    ~SharedPointer()
    {
        m_pData = nullptr;
        if (m_pInternal) {
            m_pInternal->delOwner();
            m_pInternal->delRef();
            m_pInternal = nullptr;
        }
    }

    SharedPointer& operator=(const SharedPointer& sp)
    {
        if (this != &sp)
        {
            if (m_pInternal) {
                m_pInternal->delOwner();
                m_pInternal->delRef();
            }
            m_pData = sp.m_pData;
            m_pInternal = sp.m_pInternal;
            if (m_pInternal) {
                m_pInternal->addRef();
                m_pInternal->addOwner();
            }
        }
        return *this;
    }

    SharedPointer& operator=(SharedPointer&& sp)
    {
        if (this != &sp)
        {
            if (m_pInternal) {
                m_pInternal->delOwner();
                m_pInternal->delRef();
            }
            m_pData = sp.m_pData;
            m_pInternal = sp.m_pInternal;
            sp.m_pData = nullptr;
            sp.m_pInternal = nullptr;
        }
        return *this;
    }

    operator bool() const
    {
        return m_pData != nullptr;
    }

    void reset(T*const pData = nullptr)
    {
        m_pData = pData;
        if (m_pInternal) {
            m_pInternal->delOwner();
            m_pInternal->delRef();
        }
        m_pInternal = pData ? new SharedPointerInternal(pData) : nullptr;
    }

    T* operator->() { return m_pData; }
    const T* operator->() const { return m_pData; }
    T& operator*() { return *m_pData; }
    const T& operator*() const { return *m_pData; }
    T* get() { return m_pData; }
    const T* get() const { return m_pData; }

    friend class WeakPointer<T>;
};

///////////////////////////////////////////////////////////////////////////////
template<typename T>
class WeakPointer final
{
    T* m_pData;
    typename SharedPointer<T>::SharedPointerInternal* m_pInternal;
    bool m_bLocked;

public:
    WeakPointer() : m_pData(nullptr), m_pInternal(nullptr), m_bLocked(false)
    {
    }

    WeakPointer(const SharedPointer<T>& sp) : m_pData(sp.m_pData), m_pInternal(sp.m_pInternal), m_bLocked(false)
    {
        if (m_pInternal) {
            m_pInternal->addRef();
        }
    }

    WeakPointer(const WeakPointer& wp) : m_pData(wp.m_pData), m_pInternal(wp.m_pInternal), m_bLocked(wp.m_bLocked)
    {
        if (m_pInternal) {
            m_pInternal->addRef();
            if (m_bLocked) {
                m_pInternal->addOwner();
            }
        }
    }

    WeakPointer(WeakPointer&& wp) : m_pData(wp.m_pData), m_pInternal(wp.m_pInternal), m_bLocked(wp.m_bLocked)
    {
        wp.m_pData = nullptr;
        wp.m_pInternal = nullptr;
        wp.m_bLocked = false;
    }

    ~WeakPointer()
    {
        if (m_pInternal) {
            if (m_bLocked) {
                m_pInternal->delOwner();
            }
            m_pInternal->delRef();
        }
        m_pData = nullptr;
        m_pInternal = nullptr;
        m_bLocked = false;
    }

    WeakPointer& operator=(const WeakPointer& wp)
    {
        if (this != &wp)
        {
            if (m_pInternal) {
                if (m_bLocked) {
                    m_pInternal->delOwner();
                }
                m_pInternal->delRef();
            }
            m_pData = wp.m_pData;
            m_pInternal = wp.m_pInternal;
            m_bLocked = wp.m_bLocked;
            if (m_pInternal) {
                m_pInternal->addRef();
                if (m_bLocked) {
                    m_pInternal->addOwner();
                }
            }
        }
        return *this;
    }

    WeakPointer& operator=(WeakPointer&& wp)
    {
        if (this != &wp)
        {
            if (m_pInternal) {
                if (m_bLocked) {
                    m_pInternal->delOwner();
                }
                m_pInternal->delRef();
            }
            m_pData = wp.m_pData;
            m_pInternal = wp.m_pInternal;
            m_bLocked = wp.m_bLocked;
            wp.m_pData = nullptr;
            wp.m_pInternal = nullptr;
            wp.m_bLocked = false;
        }
        return *this;
    }

    operator bool() const
    {
        return m_bLocked && m_pData;
    }

    T* operator->() { return m_bLocked ? m_pData : nullptr; }
    const T* operator->() const { return m_bLocked ? m_pData : nullptr; }
    T& operator*() { return *(m_bLocked ? m_pData : nullptr); }
    const T& operator*() const { return *(m_bLocked ? m_pData : nullptr); }
    T* get() { return m_bLocked ? m_pData : nullptr; }
    const T* get() const { return m_bLocked ? m_pData : nullptr; }

    bool lock()
    {
        if (!m_bLocked)
        {
            m_bLocked = m_pInternal ? (m_pInternal->addOwner() > 0) : false;
        }
        return m_bLocked;
    }

    bool unlock()
    {
        if (m_bLocked)
        {
            m_bLocked = false;
            if (m_pInternal)
            {
                m_pInternal->delOwner();
            }
        }
        return !m_bLocked;
    }

    bool isLocked() const
    {
        return m_bLocked;
    }

    SharedPointer<T> toSharedPointer() const
    {
        if (m_pData && m_pInternal->addOwner())
        {
            m_pInternal->addRef();

            SharedPointer<T> sp;
            sp.m_pData = m_pData;
            sp.m_pInternal = m_pInternal;
            return sp;
        }

        return SharedPointer<T>();
    }
};

#endif