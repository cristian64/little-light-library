#include "catch.hpp"

#include "TickingThread.h"

namespace
{
    class CustomTick : public Tick
    {
    public:
        CustomTick() { m_nType = 1729; s_nCount.fetch_add(1); }
        virtual ~CustomTick() { s_nCount.fetch_sub(1); }

        static std::atomic_int s_nCount;

    private:
        DISABLE_COPY(CustomTick)
    };

    /*static */std::atomic_int CustomTick::s_nCount(0);

    class TesterTickingThread : public TickingThread
    {
    public:
        void tick(Tick*const pTick) override
        {
            UNUSED(pTick);
            msleep(10);
        }
    };
}

TEST_CASE("Basic tests for the ticking thread", "[TickingThread]")
{
    TesterTickingThread thread;
    thread.start();
    REQUIRE(thread.waitUntilRunning());
    for (int i = 0; i < 100; ++i)
    {
        thread.post(new Tick());
    }
    REQUIRE(!thread.wait(100));
    Thread::msleep(500);
    thread.quit();
    REQUIRE(thread.wait(100));
}

TEST_CASE("Ticking thread with a custom tick", "[TickingThread]")
{
    REQUIRE(CustomTick::s_nCount == 0);

    TesterTickingThread thread;
    for (int i = 0; i < 50; ++i)
    {
        thread.post(new CustomTick());
        thread.post(new Tick());
    }
    REQUIRE(CustomTick::s_nCount == 50);
    thread.start();
    REQUIRE(thread.waitUntilRunning());
    for (int i = 0; i < 50; ++i)
    {
        thread.post(new CustomTick());
        thread.post(new Tick());
    }
    REQUIRE(!thread.wait(100));
    Thread::msleep(500);
    thread.quit();
    REQUIRE(thread.wait(100));

    REQUIRE(CustomTick::s_nCount == 0);
}
