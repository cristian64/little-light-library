#include "catch.hpp"

#include "Mutex.h"
#include "Thread.h"

TEST_CASE("Try to lock an already-locked mutex", "[Mutex]")
{
    Mutex mutex;
    mutex.lock();
    REQUIRE(!mutex.tryLock(100));
    mutex.unlock();
    REQUIRE(mutex.tryLock(100));
}

TEST_CASE("Try to lock an unlocked mutex", "[Mutex]")
{
    Mutex mutex;
    mutex.lock();
    mutex.unlock();
    REQUIRE(mutex.tryLock(100));
}

TEST_CASE("Try to lock a mutex currently locked by a locker", "[Mutex][MutexLocker]")
{
    Mutex mutex;
    MutexLocker lock(mutex);
    REQUIRE(!mutex.tryLock(100));
}

TEST_CASE("Lock mutex with a locker and confirm it's been locked", "[Mutex][MutexLocker]")
{
    Mutex mutex;
    int nFails = 0;
    for (int i = 0; i < 10; ++i)
    {
        MutexLocker lock(mutex);
        nFails += !mutex.tryLock(0);
    }
    CHECK(nFails == 10);
    REQUIRE(mutex.tryLock(100));
}

namespace
{
    class TesterThread : public Thread
    {
        Mutex& m_mutex;
    public:
        TesterThread(Mutex& mutex) : m_mutex(mutex) {}
        void run() override
        {
            msleep(100);
            m_mutex.unlock();
        }
    };
}

TEST_CASE("Cause a double lock and wait for a second thread to free main thread", "[Mutex][MutexLocker][Thread]")
{
    for (int i = 0; i < 10; ++i)
    {
        Mutex mutex;
        MutexLocker lock(mutex);
        TesterThread thread(mutex);
        thread.start();
        REQUIRE(!mutex.tryLock(10));
        mutex.lock();
        REQUIRE(!mutex.tryLock(10));
        thread.quit();
        thread.wait();
    }
}

namespace
{
    class TesterThread2 : public Thread
    {
        Mutex& m_mutex;
        std::vector<int>& m_v;

    public:
        TesterThread2(Mutex& mutex, std::vector<int>& v) : m_mutex(mutex), m_v(v) {}
        void run() override
        {
            while (!isCancelled())
            {
                msleep(10);

                MutexLocker lock(m_mutex);
                const int next = m_v[0] + 1;
                for (unsigned long long int i = 0; i < m_v.size(); ++i)
                {
                    m_v[i] = next;
                }
            }
        }
    };
}

TEST_CASE("Main thread and secondary thread compete to read and write data", "[Mutex][MutexLocker][Thread]")
{
    Mutex mutex;
    std::vector<int> v(1920*1080, 0);
    TesterThread2 thread(mutex, v);
    thread.start();

    bool bExpected = true;
    while (bExpected)
    {
        Thread::msleep(10);

        MutexLocker lock(mutex);

        bool bExpected = true;
        const int current = v[0];
        for (unsigned long long int i = 0; i < v.size() && bExpected; ++i)
        {
            bExpected = current == v[i];
        }

        if (current > 50)
        {
            break;
        }
    }

    REQUIRE(bExpected);

    thread.quit();
    REQUIRE(thread.wait());
}
