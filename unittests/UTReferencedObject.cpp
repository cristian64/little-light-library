#include "catch.hpp"

#include "ReferencedObject.h"

namespace
{
    std::atomic_int g_nAliveCount(0);
}

namespace
{
    class FullHDFrame : public ReferencedObject
    {
    public:
        FullHDFrame() : m_vBuffer(1920 * 1080, 0)
        {
            g_nAliveCount.fetch_add(1);
        }

        ~FullHDFrame()
        {
            g_nAliveCount.fetch_sub(1);
        }

        int getPixelCount() const { return (int)m_vBuffer.size(); }

    protected:
        void onDereferenced() override
        {
            delete this;
        }

    private:
        DISABLE_COPY(FullHDFrame)

        std::vector<int> m_vBuffer;
    };

    typedef ReferencedObjectContainer<FullHDFrame> FullHDFrameContainer;
}

TEST_CASE("Basic tests for the referenced object", "[ReferencedObject]")
{
    {
        FullHDFrameContainer c(new FullHDFrame);
        REQUIRE(g_nAliveCount == 1);
    }
    REQUIRE(g_nAliveCount == 0);

    for (int i = 0; i < 3; ++i)
    {
        {
            FullHDFrameContainer c(new FullHDFrame);
            REQUIRE(g_nAliveCount == 1);
            FullHDFrameContainer c2(c);
            FullHDFrameContainer c3;
            c3 = c;
            FullHDFrameContainer c4;
            c4 = c2;
            REQUIRE(g_nAliveCount == 1);
            REQUIRE(c->getPixelCount() == 1920 * 1080);
        }
        REQUIRE(g_nAliveCount == 0);
    }

    {
        FullHDFrameContainer c;
        REQUIRE(!c);
        c = new FullHDFrame;
        REQUIRE(c);
        REQUIRE(c->getPixelCount() == 1920 * 1080);
    }

    {
        FullHDFrameContainer c;
        {
            FullHDFrameContainer c2(new FullHDFrame);
            c = c2;
        }
        REQUIRE(c);
        REQUIRE(c->getPixelCount() == 1920 * 1080);
    }
    REQUIRE(g_nAliveCount == 0);

    {
        FullHDFrameContainer c = new FullHDFrame;
        FullHDFrameContainer c2(std::move(c));
        REQUIRE(!c);
        REQUIRE(c2);
        c = std::move(c2);
        REQUIRE(c);
        REQUIRE(!c2);
    }
}
