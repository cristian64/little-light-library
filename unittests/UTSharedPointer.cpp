#include "catch.hpp"

#include "SharedPointer.h"
#include "Thread.h"
#include "FairMutex.h"

TEST_CASE("Basic tests for shared pointer and weak pointer", "[SharedPointer]")
{
    WeakPointer<std::string> weakkkk;
    for (unsigned int i = 1; i < 10; ++i)
    {
        {
            SharedPointer<std::string> spStr(new std::string("this is not a test"));
            SharedPointer<std::string> copy(spStr);
            copy.reset(new std::string("this is a test"));

            SharedPointer<std::string> another;
            another = copy;

            WeakPointer<std::string> weak(another);
            weakkkk = another;
            //weakkkk.lock();

            //Log() << spStr->c_str() << another->c_str();
            //Log() << *spStr << *another;

            REQUIRE(weak.lock());
            REQUIRE(weak.isLocked());
            if (weak.isLocked()) {
                //Log() << *weak;
                REQUIRE(weak.unlock());
            }

            Thread::msleep(40);
        }
        if (weakkkk.isLocked() || weakkkk.lock())
        {
            const std::string strCopy = *weakkkk;
            REQUIRE(strCopy == "this is a test");
        }

        WeakPointer<std::string> weakkkkkkkk(weakkkk);
        weakkkkkkkk = weakkkk;
    }
}



namespace
{
    class TesterThread : public Thread
    {
    public:
        WeakPointer<std::string> m_wp;
        bool m_bShouldLock;
        int m_nErrors;

        TesterThread() : m_nErrors(0) {}

        void run() override
        {
            while (!isCancelled())
            {
                if (m_bShouldLock)
                {
                    m_nErrors += m_wp.isLocked();
                    m_nErrors += !m_wp.lock();
                    m_nErrors += !m_wp.isLocked();
                    if (m_wp.isLocked())
                    {
                        m_nErrors += *m_wp != "this is another test";
                    }
                    m_nErrors += !m_wp.unlock();

                    SharedPointer<std::string> sp = m_wp.toSharedPointer();
                    m_nErrors += !sp;
                }
                else
                {
                    m_nErrors += m_wp.isLocked();
                    m_nErrors += m_wp.lock();
                    m_nErrors += m_wp.isLocked();

                    SharedPointer<std::string> sp = m_wp.toSharedPointer();
                    m_nErrors += sp;
                }
            }
        }
    };
}

TEST_CASE("Multi-threaded tests for shared pointer and weak pointer", "[SharedPointer]")
{
    {
        SharedPointer<FairMutex> spMutex(new FairMutex());
        const SharedPointer<std::string> sp(new std::string("this is another test"));
        const SharedPointer<std::string> spCopy(sp);
        SharedPointer<std::string> spAnotherCopy;
        spAnotherCopy = spCopy;

        REQUIRE(spCopy);
        REQUIRE(spAnotherCopy.operator->() == sp.operator->());
        REQUIRE(!!spAnotherCopy);

        {
            std::vector<TesterThread> threads(100);
            for (unsigned int i = 0; i < threads.size(); ++i) {
                threads[i].m_bShouldLock = true;
                threads[i].m_wp = spAnotherCopy;
                threads[i].start();
            }
            Thread::msleep(500);
            for (unsigned int i = 0; i < threads.size(); ++i) {
                threads[i].quit();
            }
            for (unsigned int i = 0; i < threads.size(); ++i) {
                REQUIRE(threads[i].wait(500));
                REQUIRE(threads[i].m_nErrors == 0);
            }
        }
    }

    {
        SharedPointer<std::string> sp(new std::string("this is another test"));

        std::vector<TesterThread> threads(100);
        for (unsigned int i = 0; i < threads.size(); ++i) {
            threads[i].m_bShouldLock = false;
            threads[i].m_wp = sp;
        }
        sp.reset();
        for (unsigned int i = 0; i < threads.size(); ++i) {
            threads[i].start();
        }

        Thread::msleep(500);
        for (unsigned int i = 0; i < threads.size(); ++i) {
            threads[i].quit();
        }
        for (unsigned int i = 0; i < threads.size(); ++i) {
            REQUIRE(threads[i].wait(500));
            REQUIRE(threads[i].m_nErrors == 0);
        }
    }
}