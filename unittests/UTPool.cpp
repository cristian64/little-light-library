#include "catch.hpp"

#include "Pool.h"
#include "WaitableObject.h"
#include "SyncedQueue.h"
#include "Thread.h"

TEST_CASE("Basic tests for the content pool", "[Pool]")
{
    for (int i = 0; i < 10; ++i)
    {
        PoolContainer contentPoolContainer(new Pool);
        REQUIRE(contentPoolContainer);

        PoolContentContainer content;
        REQUIRE(!contentPoolContainer->getContent(content, 1));
        REQUIRE(!content);

        contentPoolContainer->addContent(new PoolContent);
        REQUIRE(contentPoolContainer->getContent(content, 10));
        REQUIRE(content);
        REQUIRE(!contentPoolContainer->getContent(content));
        content = nullptr;
        REQUIRE(!content);
        REQUIRE(contentPoolContainer->getContent(content));
        REQUIRE(content);
        content = nullptr;

        size_t nInside = 0, nOutside = 0;
        contentPoolContainer->getPoolStatus(nInside, nOutside);
        REQUIRE(contentPoolContainer->cancel());
        contentPoolContainer = nullptr;
    }
}

namespace
{
    std::atomic_int g_nAliveCount(0);

    class FullHDFrame : public PoolContent, public WaitableObject
    {
    public:
        FullHDFrame() : m_vBuffer(1920 * 1080, 0)
        {
            g_nAliveCount.fetch_add(1);
        }
        ~FullHDFrame()
        {
            g_nAliveCount.fetch_sub(1);
        }
        int getPixelCount() const { return (int)m_vBuffer.size(); }
        void fill(const int pixel)
        {
            for (size_t i = 0; i < m_vBuffer.size(); ++i)
            {
                m_vBuffer[i] = pixel;
            }
        }
        const int* getPixels() const
        {
            return m_vBuffer.data();
        }

    private:
        DISABLE_COPY(FullHDFrame)

        std::vector<int> m_vBuffer;
    };

    typedef ReferencedObjectContainer<FullHDFrame> FullHDFrameContainer;
}

TEST_CASE("Extended tests for the content pool", "[Pool]")
{
    {
        PoolContainer contentPoolContainer(new Pool);
        REQUIRE(contentPoolContainer);

        PoolContentContainer content;
        REQUIRE(!contentPoolContainer->getContent(content, 10));
        REQUIRE(!content);

        contentPoolContainer->addContent(new FullHDFrame);
        REQUIRE(contentPoolContainer->getContent(content, 10));
        REQUIRE(content);
        REQUIRE(!contentPoolContainer->getContent(content));
        content = nullptr;
        REQUIRE(!content);
        REQUIRE(contentPoolContainer->getContent(content));
        REQUIRE(content);
        content = nullptr;

        size_t nInside = 0, nOutside = 0;
        contentPoolContainer->getPoolStatus(nInside, nOutside);
        REQUIRE(contentPoolContainer->cancel());
        contentPoolContainer = nullptr;
    }
}

namespace
{
    class WriterThread : public Thread
    {
    public:
        void quit() override
        {
            Thread::quit();
        }

        void enqueue(const FullHDFrameContainer& c)
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_q.push_back(c);
        }

    protected:
        void run() override
        {
            while (!isCancelled())
            {
                std::lock_guard<std::mutex> lock(m_mutex);

                if (m_q.size())
                {
                    FullHDFrameContainer c = m_q.front();
                    m_q.pop_front();
                    if (c) {
                        msleep(1);
                        c->fill(10);
                        c->decCount();
                    }
                }
            }

            std::lock_guard<std::mutex> lock(m_mutex);
            m_q.clear();
        }

        std::mutex m_mutex;
        std::list<FullHDFrameContainer> m_q;
    };

    class ReaderThread : public Thread
    {
    public:
        void quit() override
        {
            Thread::quit();
            m_q.cancel();
        }

        void enqueue(const FullHDFrameContainer& c)
        {
            m_q.push(c);
        }

        int m_nMismatches;
        bool m_bDoNotWaitForWriteCompletion;

    protected:
        void run() override
        {
            m_nMismatches = 0;
            while (!isCancelled())
            {
                FullHDFrameContainer c;
                if (m_q.waitForItem(c) && c)
                {
                    if (m_bDoNotWaitForWriteCompletion || c->waitForObject(100))
                    {
                        for (int i = 0; i < c->getPixelCount(); ++i)
                        {
                            m_nMismatches += c->getPixels()[i] != 10;
                        }
                    }
                }
            }
        }

        SyncedQueue<FullHDFrameContainer> m_q;
    };
}

TEST_CASE("Multi-threaded tests for the content pool", "[Thread][WaitableObject][Pool]")
{
    Thread::setCurrentThreadName("Main thread");
    for (int j = 0; j < 2; ++j)
    {
        WriterThread writer;
        writer.start();
        REQUIRE(writer.waitUntilRunning(1000));
        writer.setName("Writer thread");

        ReaderThread reader;
        reader.m_bDoNotWaitForWriteCompletion = false;
        reader.start();
        REQUIRE(reader.waitUntilRunning(1000));
        reader.setName("Reader thread");

        ReaderThread reader2;
        reader2.m_bDoNotWaitForWriteCompletion = true;
        reader2.start();
        REQUIRE(reader2.waitUntilRunning(1000));
        reader2.setName("Reader2 thread");

        PoolContainer contentPoolContainer(new Pool);
        for (int i = 0; i < 5; ++i) {
            FullHDFrame* pFrame = new FullHDFrame;
            contentPoolContainer->addContent(pFrame);
        }

        for (int i = 0; i < 50; ++i)
        {
            FullHDFrameContainer c;
            REQUIRE(contentPoolContainer->getContent<FullHDFrameContainer>(c, 100));
            REQUIRE(c);
            if (c)
            {
                c->incCount();
                writer.enqueue(c);
                reader.enqueue(c);
                reader2.enqueue(c);
            }
        }
        REQUIRE(contentPoolContainer->cancel(1000));

        reader.quit();
        REQUIRE(reader.wait(1000));
        REQUIRE(reader.m_nMismatches == 0);

        reader2.quit();
        REQUIRE(reader2.wait(1000));
        REQUIRE(reader2.m_nMismatches > 0);

        writer.quit();
        REQUIRE(writer.wait(1000));
    }
}
