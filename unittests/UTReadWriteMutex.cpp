#include "catch.hpp"

#include "ReadWriteMutex.h"
#include "Thread.h"

TEST_CASE("Try to lock for write a mutex that is already locked for read", "[ReadWriteMutex]")
{
    ReadWriteMutex mutex;
    mutex.lockForRead();
    REQUIRE(!mutex.tryLockForWrite(100));
    mutex.lockForRead();
    mutex.lockForRead();
    REQUIRE(!mutex.tryLockForWrite(100));
    mutex.lockForRead();
    mutex.unlockForRead();
    mutex.unlockForRead();
    mutex.unlockForRead();
    mutex.unlockForRead();
    REQUIRE(mutex.tryLockForWrite(100));
}

TEST_CASE("Try to lock for write an unlocked mutex", "[ReadWriteMutex]")
{
    ReadWriteMutex mutex;
    mutex.lockForWrite();
    mutex.unlockForWrite();
    REQUIRE(mutex.tryLockForWrite(100));
}

TEST_CASE("Try to lock for read/write a mutex currently locked by a locker",
          "[ReadWriteMutex][ReadMutexLocker][WriteMutexLocker]")
{
    {
        ReadWriteMutex mutex;
        WriteMutexLocker lock(mutex);
        REQUIRE(!mutex.tryLockForWrite(100));
    }

    {
        ReadWriteMutex mutex;
        ReadMutexLocker lock(mutex);
        REQUIRE(!mutex.tryLockForWrite(100));
    }

    {
        ReadWriteMutex mutex;
        WriteMutexLocker lock(mutex);
        REQUIRE(!mutex.tryLockForRead(100));
    }

    {
        ReadWriteMutex mutex;
        ReadMutexLocker lock(mutex);
        REQUIRE(mutex.tryLockForRead(100));
    }
}

TEST_CASE("Lock mutex for write with a locker and confirm it's been locked",
          "[ReadWriteMutex][WriteMutexLocker]")
{
    ReadWriteMutex mutex;
    int nFails = 0;
    for (int i = 0; i < 10; ++i)
    {
        WriteMutexLocker lock(mutex);
        nFails += !mutex.tryLockForRead(0);
        nFails += !mutex.tryLockForRead(5);
        nFails += !mutex.tryLockForWrite(0);
        nFails += !mutex.tryLockForWrite(5);
    }
    CHECK(nFails == 40);
    CHECK(mutex.tryLockForWrite(100));
    mutex.unlockForWrite();
    REQUIRE(mutex.tryLockForRead(100));
    mutex.unlockForRead();
}

namespace
{
    class TesterThread : public Thread
    {
        ReadWriteMutex& m_mutex;
    public:
        TesterThread(ReadWriteMutex& mutex) : m_mutex(mutex) {}
        void run() override
        {
            msleep(100);
            m_mutex.unlockForWrite();
        }
    };
}

TEST_CASE("Cause a double lock for write and wait for a second thread to liberate main thread",
          "[ReadWriteMutex][WriteMutexLocker][Thread]")
{
    for (int i = 0; i < 10; ++i)
    {
        ReadWriteMutex mutex;
        WriteMutexLocker lock(mutex);
        TesterThread thread(mutex);
        thread.start();
        REQUIRE(!mutex.tryLockForWrite(10));
        mutex.lockForWrite();
        REQUIRE(!mutex.tryLockForWrite(10));
        thread.quit();
        thread.wait();
    }
}

namespace
{
    class TesterThread2 : public Thread
    {
        ReadWriteMutex& m_mutex;
        std::vector<int>& m_v;
        int m_nErrors;

    public:
        TesterThread2(ReadWriteMutex& mutex, std::vector<int>& v) : m_mutex(mutex), m_v(v), m_nErrors(0) {}
        void run() override
        {
            while (!isCancelled())
            {
                msleep(50);

                ReadMutexLocker lock(m_mutex);
                const int current = m_v[0];
                for (unsigned long long int i = 0; i < m_v.size(); ++i)
                {
                    m_nErrors += current != m_v[i];
                }

                if (current > 50)
                {
                    quit();
                }
            }
        }

        int getErrors() const { return m_nErrors; }
    };
}

TEST_CASE("Main thread and secondary threads compete to write and read data",
          "[ReadWriteMutex][ReadMutexLocker][WriteMutexLocker][Thread]")
{
    ReadWriteMutex mutex;
    std::vector<int> v(1920*1080, 0);

    std::vector<TesterThread2*> threads(10, nullptr);
    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i] = new TesterThread2(mutex, v);
        threads[i]->start();
        REQUIRE(threads[i]->waitUntilRunning(100));
        threads[i]->setName("thread." + std::to_string(i));
    }

    for (int it = 0; it < 52; ++it)
    {
        Thread::msleep(10);

        WriteMutexLocker lock(mutex);

        const int next = v[0] + 1;
        for (unsigned long long int i = 0; i < v.size(); ++i) {
            v[i] = next;
        }
    }

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        REQUIRE(threads[i]->wait(50));
        REQUIRE(threads[i]->getErrors() == 0);
        delete threads[i];
        threads[i] = nullptr;
    }
}
