#include "catch.hpp"

#include "WaitableObject.h"
#include "Thread.h"

namespace
{
    class FullHDFrame : public WaitableObject
    {
    public:
        FullHDFrame() : m_vBuffer(1920 * 1080, 0)
        {
        }

        ~FullHDFrame()
        {
        }

        void fill(const int pixel)
        {
            for (size_t i = 0; i < m_vBuffer.size(); ++i)
            {
                m_vBuffer[i] = pixel;
            }
        }

    private:
        DISABLE_COPY(FullHDFrame)

        std::vector<int> m_vBuffer;
    };
}

TEST_CASE("Basic tests for the waitable object", "[WaitableObject]")
{
    FullHDFrame frame;
    frame.incCount();
    REQUIRE(!frame.waitForObject(0));
    REQUIRE(!frame.waitForObject(100));
    frame.decCount();
    REQUIRE(frame.waitForObject());
    REQUIRE(frame.waitForObject(100));
}

class TesterThread : public Thread
{
public:
    FullHDFrame* pFrame;
    void run() override
    {
        for (int i = 0; i < 5; ++i)
        {
            Thread::msleep(100);
            pFrame->decCount();
        }
    }
};

TEST_CASE("Threaded test for the waitable object", "[Thread][WaitableObject]")
{
    FullHDFrame frame;
    frame.incCount(5);

    TesterThread thread;
    thread.pFrame = &frame;
    thread.start();
    thread.waitUntilRunning();
    REQUIRE(!frame.waitForObject(100));
    REQUIRE(frame.waitForObject(500));
    thread.quit();
    REQUIRE(thread.wait(1000));
}
