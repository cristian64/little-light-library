#include "catch.hpp"

#include "Random.h"
#include <vector>
#include <limits>

TEST_CASE("Basic tests for the Random class", "[Random]")
{
    Random random;

    const size_t nSize = 1000;
    std::vector<int> histogram(nSize, 0);

    const size_t nCount = 100000000;
    for (size_t i = 0; i < nCount; ++i)
    {
        const int nRandomIndex = random.rand() % nSize;
        ++histogram[nRandomIndex];
    }

    // Find min and max.
    int nMax = std::numeric_limits<int>::min();
    int nMin = std::numeric_limits<int>::max();
    for (size_t i = 0; i < nSize; ++i)
    {
        nMax = std::max(nMax, histogram[i]);
        nMin = std::min(nMin, histogram[i]);
    }

    const long double dRatio = nMin / (long double)(nMax);
    INFO("Min: " << nMin);
    INFO("Max: " << nMax);
    REQUIRE(dRatio > 0.95);
}
