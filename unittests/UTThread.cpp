#include "catch.hpp"

#include "Thread.h"
#include "Mutex.h"
#include "HighResTimer.h"

namespace
{
    class TesterThread : public Thread
    {
    public:
        void run() override
        {
            for (int i = 0; i < 15; ++i) {
                msleep(10);
                s_nCount.fetch_add(1);
                yield();
            }
        }

        static std::atomic_int s_nCount;
    };

    /*static */std::atomic_int TesterThread::s_nCount(0);
}

TEST_CASE("Basic tests for the thread", "[Thread]")
{
    TesterThread::s_nCount = 0;
    TesterThread thread;
    REQUIRE(!thread.isCancelled());
    REQUIRE(!thread.isRunning());
    REQUIRE(thread.start());
    REQUIRE(thread.waitUntilRunning());
    REQUIRE(!thread.wait(100));
    REQUIRE(thread.isRunning());
    thread.quit();
    REQUIRE(thread.isCancelled());
    REQUIRE(thread.wait(100));
    REQUIRE(TesterThread::s_nCount == 15);
}

TEST_CASE("Thread sleep functions", "[Thread][!mayfail][!nonportable]")
{
    {
        HighResTimer timer;
        {
            Thread::msleep(100);
            const auto nElapsedMs = timer.periodms();
            CHECK(99 <= nElapsedMs);
            CHECK(nElapsedMs <= 105);
        }

        {
            Thread::msleep(200);
            const auto nElapsedMs = timer.periodms();
            CHECK(298 <= nElapsedMs);
            CHECK(nElapsedMs <= 305);
        }
    }

    {
        HighResTimer timer;
        Thread::usleep(3000);
        const auto nElapsedMs = timer.periodms();
        CHECK(2 <= nElapsedMs);
        CHECK(nElapsedMs <= 4);
    }

    {
        HighResTimer timer;
        Thread::usleep(500);
        const auto nElapsedUs = timer.periodus();
        CHECK(495 <= nElapsedUs);
        CHECK(nElapsedUs <= 600);
    }

    {
        HighResTimer timer;
        Thread::usleep(2500);
        const auto nElapsedUs = timer.periodus();
        CHECK(2495 <= nElapsedUs);
        CHECK(nElapsedUs <= 2600);
    }

    for (int i = 0; i < 5; ++i)
    {
        HighResTimer timer;
        Thread::usleep(300);
        const auto nElapsedUs = timer.periodus();
        CHECK(295 <= nElapsedUs);
        CHECK(nElapsedUs <= 400);
    }

    {
        HighResTimer timer;
        for (int i = 0; i < 10; ++i)
        {
            Thread::usleep(500);
        }
        const auto nElapsedUs = timer.periodus();
        CHECK(4995 <= nElapsedUs);
        CHECK(nElapsedUs <= 6000);
    }

    {
        HighResTimer timer;
        for (int i = 0; i < 10; ++i)
        {
            Thread::nsleep(1000000);
        }
        const auto nElapsedMs = timer.periodms();
        CHECK(9 <= nElapsedMs);
        CHECK(nElapsedMs <= 15);
    }

    {
        HighResTimer timer;
        for (int i = 0; i < 10; ++i)
        {
            Thread::nsleep(1000000);
        }
        const auto nElapsedNs = timer.periodns();
        CHECK(10000000 <= nElapsedNs);
        CHECK(nElapsedNs <= 140000000);
    }
}

namespace
{
    // Using this mutex to be able to invoke REQUIRE(...) from
    // different threads, since it isn't thread safe.
    Mutex g_mutex;
    std::atomic_int g_nChildrenCount(0);

    class ChildThread : public Thread
    {
    public:
        void run() override
        {
            msleep(250);
            g_nChildrenCount.fetch_add(1);
            msleep(250);
        }
    };

    class ParentThread : public Thread
    {
    public:
        void run() override
        {
            std::vector<ChildThread> threads(5);
            for (auto& thread : threads)
            {
                const bool bStart = thread.start();
                MutexLocker lock(g_mutex);
                REQUIRE(bStart);
            }

            for (const auto& thread : threads)
            {
                while(!thread.isRunning()) {}
            }

            for (auto& thread : threads)
            {
                const bool bWait = thread.wait();
                MutexLocker lock(g_mutex);
                REQUIRE(bWait);
                REQUIRE(thread.isFinished());
            }
        }
    };
}

TEST_CASE("Nested threads", "[Thread]")
{
    {
        MutexLocker lock(g_mutex);
        REQUIRE(g_nChildrenCount == 0);
    }

    std::vector<ParentThread> threads(10);
    for (auto& thread : threads)
    {
        const bool bStart = thread.start();
        MutexLocker lock(g_mutex);
        REQUIRE(bStart);
    }

    for (const auto& thread : threads)
    {
        while(!thread.isRunning()) {}
    }

    for (auto& thread : threads)
    {
        const bool bWait = thread.wait();
        MutexLocker lock(g_mutex);
        REQUIRE(bWait);
        REQUIRE(thread.isFinished());
    }

    {
        MutexLocker lock(g_mutex);
        REQUIRE(g_nChildrenCount == 50);
    }
}

namespace
{
    class NamedThread : public Thread
    {
    public:
        void run() override
        {
            while (!isCancelled())
            {
                msleep(40);
            }
        }
    };
}

TEST_CASE("Set and get thread name", "[Thread]")
{
    for (int i = 0; i < 5; ++i)
    {
        NamedThread thread;

        REQUIRE(!thread.waitUntilRunning(100));
        REQUIRE(!thread.waitUntilRunning(0));
        REQUIRE(thread.start());
        REQUIRE(thread.waitUntilRunning(100));
        const std::string strThreadName = "Thread" + std::to_string(i);
        REQUIRE(thread.setName(strThreadName));
        REQUIRE(!thread.wait(250));
        REQUIRE(thread.getName() == strThreadName);

        thread.quit();
        REQUIRE(thread.wait(500));
        REQUIRE(thread.isFinished());
        REQUIRE(!thread.isRunning());
        REQUIRE(thread.isCancelled());
    }
}

namespace
{
    int g_nCount = 0;
    class UpdaterThread : public Thread
    {
    public:
        void run() override
        {
            while (!isCancelled())
            {
                msleep(10); // Termination point! Required for pthread_cancel() on Linux systems
                ++g_nCount;
            }
        }
    };
}

TEST_CASE("Terminate thread", "[Thread::terminate][Thread]")
{
    for (int i = 0; i < 10; ++i)
    {
        UpdaterThread thread;
        thread.start();
        REQUIRE(thread.waitUntilRunning());
        const std::string strThreadName = "Thread" + std::to_string(i);
        REQUIRE(thread.setName(strThreadName));
        REQUIRE(!thread.wait(250));
        REQUIRE(thread.terminate());
        Thread::msleep(100);
        const int nFinalCount = g_nCount;
        Thread::msleep(500);
        REQUIRE(nFinalCount == g_nCount);
    }
}
