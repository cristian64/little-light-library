#include "catch.hpp"

#include "FairMutex.h"
#include "Mutex.h"
#include "Thread.h"

TEST_CASE("Try to lock an already-locked fair mutex", "[FairMutex]")
{
    FairMutex mutex;
    mutex.lock();
    REQUIRE(!mutex.tryLock(100));
    mutex.unlock();
    REQUIRE(mutex.tryLock(100));
}

TEST_CASE("Try to lock an unlocked fair mutex", "[FairMutex]")
{
    FairMutex mutex;
    mutex.lock();
    mutex.unlock();
    REQUIRE(mutex.tryLock(100));
}

TEST_CASE("Try to lock a fair mutex currently locked by a locker", "[FairMutex][FairMutexLocker]")
{
    FairMutex mutex;
    FairMutexLocker lock(mutex);
    REQUIRE(!mutex.tryLock(100));
}

TEST_CASE("Lock fair mutex with a locker and confirm it's been locked", "[FairMutex][FairMutexLocker]")
{
    FairMutex mutex;
    int nFails = 0;
    for (int i = 0; i < 10; ++i)
    {
        FairMutexLocker lock(mutex);
        nFails += !mutex.tryLock(0);
    }
    CHECK(nFails == 10);
    REQUIRE(mutex.tryLock(100));
}

namespace
{
    class TesterThread : public Thread
    {
        FairMutex& m_mutex;
    public:
        TesterThread(FairMutex& mutex) : m_mutex(mutex) {}
        void run() override
        {
            msleep(100);
            m_mutex.unlock();
        }
    };
}

TEST_CASE("Cause a double lock and wait for a second thread to free main thread with a fair mutex", "[FairMutex][FairMutexLocker][Thread]")
{
    for (int i = 0; i < 10; ++i)
    {
        FairMutex mutex;
        FairMutexLocker lock(mutex);
        TesterThread thread(mutex);
        thread.start();
        REQUIRE(!mutex.tryLock(10));
        mutex.lock();
        REQUIRE(!mutex.tryLock(10));
        thread.quit();
        thread.wait();
    }
}

namespace
{
    class TesterThread2 : public Thread
    {
        FairMutex& m_mutex;
        std::vector<int>& m_v;

    public:
        TesterThread2(FairMutex& mutex, std::vector<int>& v) : m_mutex(mutex), m_v(v) {}
        void run() override
        {
            while (!isCancelled())
            {
                msleep(10);

                FairMutexLocker lock(m_mutex);
                const int next = m_v[0] + 1;
                for (unsigned long long int i = 0; i < m_v.size(); ++i)
                {
                    m_v[i] = next;
                }
            }
        }
    };
}

TEST_CASE("Main thread and secondary thread compete to read and write data with a fair mutex", "[FairMutex][FairMutexLocker][Thread]")
{
    FairMutex mutex;
    std::vector<int> v(1920*1080, 0);
    TesterThread2 thread(mutex, v);
    thread.start();

    bool bExpected = true;
    while (bExpected)
    {
        Thread::msleep(10);

        FairMutexLocker lock(mutex);

        bool bExpected = true;
        const int current = v[0];
        for (unsigned long long int i = 0; i < v.size() && bExpected; ++i)
        {
            bExpected = current == v[i];
        }

        if (current > 50)
        {
            break;
        }
    }

    REQUIRE(bExpected);

    thread.quit();
    REQUIRE(thread.wait());
}

#define MUTEX FairMutex
#define LOCKER FairMutexLocker

namespace
{
    class TesterThread3 : public Thread
    {
        MUTEX& m_mutex;
        std::vector<int>& m_v;
        std::atomic_int m_nLoops;

    public:
        TesterThread3(MUTEX& mutex, std::vector<int>& v) : m_mutex(mutex), m_v(v), m_nLoops(0) {}
        void run() override
        {
            while (!isCancelled())
            {
                LOCKER lock(m_mutex);
                m_nLoops.fetch_add(1);
                const int next = m_v[0] + 1;
                for (unsigned long long int i = 0; i < m_v.size(); ++i)
                {
                    m_v[i] = next;
                }
            }
        }

        int getLoops() const { return m_nLoops; }
    };
}

TEST_CASE("Several loops that access the same exclusive data in the same percentage", "[FairMutex][FairMutexLocker][Thread]")
{
    MUTEX mutex;
    std::vector<int> v(1920*1080, 0);

    std::vector<TesterThread3*> threads(10, nullptr);
    {
        LOCKER lock(mutex);
        for (size_t i = 0; i < threads.size(); ++i) {
            threads[i] = new TesterThread3(mutex, v);
            threads[i]->start();
        }
    }

    Thread::msleep(1000);

    for (size_t i = 0; i < threads.size(); ++i) {
        threads[i]->quit();
    }

    for (size_t i = 0; i < threads.size(); ++i) {
        REQUIRE(threads[i]->wait(100));
    }

    std::string strLoops;
    int nTotalLoops = 0;
    int nMax = std::numeric_limits<int>::min();
    int nMin = std::numeric_limits<int>::max();
    for (size_t i = 0; i < threads.size(); ++i) {
        const int nLoops = threads[i]->getLoops();
        nMax = std::max(nMax, nLoops);
        nMin = std::min(nMin, nLoops);
        nTotalLoops += nLoops;
        strLoops += std::to_string(threads[i]->getLoops()) + " ";
    }
    strLoops += "= " + std::to_string(nTotalLoops);
    INFO(strLoops);
    const size_t nAvgLoops = nTotalLoops / threads.size();

    INFO("Loops:" << nTotalLoops << " [min:" << nMin << " max:" << nMax << " avg:" << nAvgLoops << "]");
    REQUIRE(nMin + 5 >= nAvgLoops);
    REQUIRE(nAvgLoops >= nMax - 5);

    for (size_t i = 0; i < threads.size(); ++i) {
        delete threads[i];
        threads[i] = nullptr;
    }
}