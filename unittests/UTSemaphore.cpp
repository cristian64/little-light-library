#include "catch.hpp"

#include "Semaphore.h"
#include "Thread.h"

TEST_CASE("Basic tests for the semaphore", "[Semaphore]")
{
    {
        Semaphore semaphore;
        semaphore.release(10);
        semaphore.acquire(5);
        semaphore.acquire(5);
        REQUIRE(!semaphore.tryAcquire(1, 100));
    }

    {
        Semaphore semaphore(10);
        semaphore.acquire(5);
        semaphore.acquire(5);
        REQUIRE(!semaphore.tryAcquire(1, 100));
    }

    {
        Semaphore semaphore(10);
        semaphore.acquire(6);
        semaphore.release(1);
        semaphore.release(0); // dummy
        REQUIRE(!semaphore.tryAcquire(6, 100));
        semaphore.release(1);
        semaphore.acquire(0); // dummy
        REQUIRE(semaphore.tryAcquire(6, 100));
    }
}

namespace
{
    class TesterThread : public Thread
    {
        Semaphore& m_semaphore;
        const unsigned int m_nSleepIntervalMs;
        const unsigned int m_nReleaseCount;

    public:
        TesterThread(Semaphore& semaphore,
                     const unsigned int nSleepIntervalMs,
                     const unsigned int nReleaseCount) :
            m_semaphore(semaphore),
            m_nSleepIntervalMs(nSleepIntervalMs),
            m_nReleaseCount(nReleaseCount)
        {
        }
        void run() override
        {
            msleep(40);

            unsigned int nReleaseCount = m_nReleaseCount;
            while (!isCancelled() && nReleaseCount)
            {
                msleep(m_nSleepIntervalMs);

                m_semaphore.release();
                --nReleaseCount;
            }
        }
    };
}

TEST_CASE("Exhaustive semaphore test with several threads", "[Semaphore][Thread]")
{
    Semaphore semaphore;

    std::vector<TesterThread*> threads(10, nullptr);

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i] = new TesterThread(semaphore, i, 10 - i);
        threads[i]->start();
    }

    for (int i = 0; i < 55; ++i)
    {
        if (i % 2 == 0) {
            REQUIRE(semaphore.tryAcquire(1, 100));
        } else {
            semaphore.acquire();
        }
    }
    REQUIRE(!semaphore.tryAcquire(1, 0));
    REQUIRE(!semaphore.tryAcquire(1, 100));

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i]->quit();
        REQUIRE(threads[i]->wait(100));
        delete threads[i];
        threads[i] = nullptr;
    }
}
