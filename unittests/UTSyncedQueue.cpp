#include "catch.hpp"

#include "SyncedQueue.h"
#include "Thread.h"
#include <atomic>

namespace
{
    class Foo
    {
    public:
        static std::atomic_int s_nCount;
        Foo() { ++s_nCount; }
        ~Foo() { --s_nCount; }
        static void deleteFoo(const Foo*const pFoo) { if (pFoo) { delete pFoo; } }

    private:
        DISABLE_COPY(Foo)
    };

    /*static */std::atomic_int Foo::s_nCount;

    static void deleteFoo(const Foo*const pFoo)
    {
        if (pFoo) { delete pFoo; }
    }

    const std::function<void(const Foo*const)> fnDeleteFoo = [](const Foo*const pFoo) { if (pFoo) { delete pFoo; } };
}

namespace
{
    class Bar
    {
    public:
        static int s_nCount;
        Bar() { }
        ~Bar() { }
        void inc() const { ++s_nCount; }
        void dec() const { --s_nCount; }
        static void decBar(const Bar& bar) { bar.dec(); }
    };

    /*static */int Bar::s_nCount = 0;

    static void decBar(const Bar& bar)
    {
        bar.dec();
    }

    const std::function<void(const Bar&)> fnDecBar = [](const Bar& bar) { bar.dec(); };
}

TEST_CASE("Basic tests for the synced queue", "[SyncedQueue]")
{
    {
        SyncedQueue<int> q;
        for (int i = 0; i < 10; ++i)
        {
            q.push(i);
        }
        for (int i = 0; i < 10; ++i)
        {
            int val;
            REQUIRE(q.waitForItem(val));
            REQUIRE(val == i);
        }
        int val;
        REQUIRE(!q.waitForItem(val, 100));
        REQUIRE(!q.isCancelled());
        q.cancel();
        REQUIRE(q.isCancelled());
    }

    {
        SyncedQueue<int> q;
        int val;
        REQUIRE(!q.waitForItem(val, 100));
        REQUIRE(!q.isCancelled());
        q.cancel();
        REQUIRE(q.isCancelled());
        REQUIRE(!q.waitForItem(val, 100));
        REQUIRE(!q.waitForItem(val));
    }
}

TEST_CASE("Custom delete function for the synced queue", "[SyncedQueue]")
{
    {
        Foo::s_nCount = 0;
        SyncedQueue<const Foo*> q(::fnDeleteFoo);
        for (int i = 0; i < 100; ++i)
        {
            q.push(new Foo());
        }
        REQUIRE(Foo::s_nCount == 100);
        q.cancel();
        REQUIRE(Foo::s_nCount == 0);
        REQUIRE(!q.push(new Foo()));
        REQUIRE(!q.push(new Foo()));
        REQUIRE(Foo::s_nCount == 0);
    }

    {
        Foo::s_nCount = 0;
        SyncedQueue<const Foo*> q(::deleteFoo);
        for (int i = 0; i < 100; ++i)
        {
            q.push(new Foo());
        }
        REQUIRE(Foo::s_nCount == 100);
        q.cancel();
        REQUIRE(Foo::s_nCount == 0);
        REQUIRE(!q.push(new Foo()));
        REQUIRE(!q.push(new Foo()));
        REQUIRE(Foo::s_nCount == 0);
    }

    {
        Foo::s_nCount = 0;
        SyncedQueue<const Foo*> q(Foo::deleteFoo);
        for (int i = 0; i < 100; ++i)
        {
            q.push(new Foo());
        }
        REQUIRE(Foo::s_nCount == 100);
        q.cancel();
        REQUIRE(Foo::s_nCount == 0);
        REQUIRE(!q.push(new Foo()));
        REQUIRE(!q.push(new Foo()));
        REQUIRE(Foo::s_nCount == 0);
    }

    {
        SyncedQueue<Bar> q(::fnDecBar);
        for (int i = 0; i < 100; ++i)
        {
            Bar bar;
            bar.inc();
            q.push(bar);
        }
        REQUIRE(Bar::s_nCount == 100);
        q.cancel();
        REQUIRE(Bar::s_nCount == 0);
    }

    {
        SyncedQueue<Bar> q(::decBar);
        for (int i = 0; i < 100; ++i)
        {
            Bar bar;
            bar.inc();
            q.push(bar);
        }
        REQUIRE(Bar::s_nCount == 100);
        q.cancel();
        REQUIRE(Bar::s_nCount == 0);
    }

    {
        SyncedQueue<Bar> q(Bar::decBar);
        for (int i = 0; i < 100; ++i)
        {
            Bar bar;
            bar.inc();
            q.push(bar);
        }
        REQUIRE(Bar::s_nCount == 100);
        q.cancel();
        REQUIRE(Bar::s_nCount == 0);
    }
}

namespace
{
    class TesterThread : public Thread
    {
        SyncedQueue<const Foo*>& m_q;
        const unsigned int m_nSleepIntervalMs;
        const unsigned int m_nReleaseCount;

    public:
        TesterThread(SyncedQueue<const Foo*>& q,
                     const unsigned int nSleepIntervalMs,
                     const unsigned int nReleaseCount) :
            m_q(q),
            m_nSleepIntervalMs(nSleepIntervalMs),
            m_nReleaseCount(nReleaseCount)
        {
        }
        void run() override
        {
            msleep(40);

            unsigned int nReleaseCount = m_nReleaseCount;
            while (!isCancelled() && nReleaseCount)
            {
                msleep(m_nSleepIntervalMs);

                m_q.push(new Foo());
                --nReleaseCount;
            }
        }
    };
}

TEST_CASE("Exhaustive test with the synced queue and several threads", "[SyncedQueue][Thread]")
{
    Foo::s_nCount = 0;

    SyncedQueue<const Foo*> q(::fnDeleteFoo);

    std::vector<TesterThread*> threads(10, nullptr);

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i] = new TesterThread(q, i, 10 - i);
        threads[i]->start();
    }

    for (int i = 0; i < 55; ++i)
    {
        const Foo* pFoo = nullptr;
        q.waitForItem(pFoo);
        REQUIRE(pFoo);
        delete pFoo;
    }
    const Foo* pFoo = nullptr;
    REQUIRE(!q.waitForItem(pFoo, 0));
    REQUIRE(!q.waitForItem(pFoo, 100));

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i]->quit();
        REQUIRE(threads[i]->wait(100));
        delete threads[i];
        threads[i] = nullptr;
    }

    REQUIRE(Foo::s_nCount == 0);

    q.push(new Foo());
    q.push(new Foo());

    REQUIRE(Foo::s_nCount == 2);

    q.cancel();

    REQUIRE(Foo::s_nCount == 0);
}

TEST_CASE("Another exhaustive test with the synced queue and several threads", "[SyncedQueue][Thread]")
{
    Foo::s_nCount = 0;

    SyncedQueue<const Foo*> q(::fnDeleteFoo);

    std::vector<TesterThread*> threads(3, nullptr);

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i] = new TesterThread(q, 100 + 100 * i, 1);
        threads[i]->start();
    }

    for (int i = 0; i < 3; ++i)
    {
        const Foo* pFoo = nullptr;
        REQUIRE(q.waitForItem(pFoo, 160));
        delete pFoo;
    }
    const Foo* pFoo = nullptr;
    REQUIRE(!q.waitForItem(pFoo, 100));
    REQUIRE(!q.waitForItem(pFoo, 10));

    for (unsigned int i = 0; i < threads.size(); ++i)
    {
        threads[i]->quit();
        REQUIRE(threads[i]->wait(100));
        delete threads[i];
        threads[i] = nullptr;
    }

    REQUIRE(Foo::s_nCount == 0);

    q.push(new Foo());
    q.push(new Foo());

    REQUIRE(Foo::s_nCount == 2);

    q.cancel();

    REQUIRE(Foo::s_nCount == 0);
}