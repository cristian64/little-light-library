#include "catch.hpp"

#include "CircularBuffer.h"

TEST_CASE("Operations on an empty circular buffer", "[CircularBuffer]")
{
    CircularBuffer<char> cb;
    REQUIRE(cb.isEmpty());
    REQUIRE(cb.isFull());
    REQUIRE(cb.getCapacity() == 0);
    REQUIRE(cb.getFreeSpace() == 0);
    REQUIRE(cb.getCount() == 0);
    REQUIRE(cb.getRawBuffer() == nullptr);

    REQUIRE(!cb.push('a'));
    char a = '\0';
    REQUIRE(!cb.pop(a));
    REQUIRE(cb.pop(nullptr, 100) == 0);
    REQUIRE(cb.push(nullptr, 100) == 0);
}

TEST_CASE("Basic tests for a circular buffer", "[CircularBuffer]")
{
    CircularBuffer<char> cb(10);
    REQUIRE(cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 10);
    REQUIRE(cb.getCount() == 0);
    REQUIRE(cb.getRawBuffer() != nullptr);

    REQUIRE(cb.push('a'));

    REQUIRE(!cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 9);
    REQUIRE(cb.getCount() == 1);
    REQUIRE(cb.getRawBuffer() != nullptr);

    char a = '\0';
    REQUIRE(cb.pop(a));
    REQUIRE(a == 'a');

    REQUIRE(cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 10);
    REQUIRE(cb.getCount() == 0);
    REQUIRE(cb.getRawBuffer() != nullptr);

    REQUIRE(cb.push('a'));
    REQUIRE(cb.push('b'));
    REQUIRE(cb.push('c'));
    REQUIRE(cb.push('d'));

    REQUIRE(!cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 6);
    REQUIRE(cb.getCount() == 4);

    REQUIRE(cb.push('e'));
    REQUIRE(cb.push('f'));
    REQUIRE(cb.push('g'));
    REQUIRE(cb.push('h'));
    REQUIRE(cb.push('i'));

    REQUIRE(!cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 1);
    REQUIRE(cb.getCount() == 9);

    REQUIRE(cb.push('j'));

    REQUIRE(cb.getCount() == 10);
    REQUIRE(!cb.isEmpty());
    REQUIRE(cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 0);

    REQUIRE(!cb.push('j'));
}

TEST_CASE("Extended tests for a circular buffer", "[CircularBuffer]")
{
    CircularBuffer<char> cb(10);
    REQUIRE(cb.push("melissa", 7) == 7);

    REQUIRE(cb.getCount() == 7);
    REQUIRE(!cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 3);

    REQUIRE(cb.push("melissa", 7) == 3);
    REQUIRE(cb.getCount() == 10);
    char a;
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(a == 'i');
    REQUIRE(cb.getCount() == 6);
}

TEST_CASE("More tests for a circular buffer", "[CircularBuffer]")
{
    CircularBuffer<char> cb(10);
    REQUIRE(cb.push("melissa", 7) == 7);

    REQUIRE(cb.getCount() == 7);
    REQUIRE(!cb.isEmpty());
    REQUIRE(!cb.isFull());
    REQUIRE(cb.getCapacity() == 10);
    REQUIRE(cb.getFreeSpace() == 3);

    char a;
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(a == 'i');
    REQUIRE(cb.getCount() == 3);

    REQUIRE(cb.push("melissa", 7) == 7);
    REQUIRE(cb.isFull());

    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(a == 'a');
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(cb.pop(a));
    REQUIRE(a == 'a');
    REQUIRE(cb.isEmpty());
    REQUIRE(!cb.pop(a));
}

TEST_CASE("More tests for a circular buffer with loops", "[CircularBuffer]")
{
    CircularBuffer<char> cb(73);

    for (int i = 0; i < 200; ++i)
    {
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));

        char c;
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(c == 'y');
    }

    for (int i = 0; i < 200; ++i)
    {
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));

        char buffer[100];
        REQUIRE(cb.pop(buffer, 12) == 12);
        REQUIRE(memcmp(buffer, "qwertyqwerty", 12) == 0);
    }

    cb.clear();
    REQUIRE(cb.isEmpty());
    REQUIRE(!cb.isFull());

    for (int i = 0; i < 200; ++i)
    {
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));

        char c;
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(cb.pop(c));
        REQUIRE(c == 'y');
    }

    for (int i = 0; i < 200; ++i)
    {
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));

        char buffer[100];
        REQUIRE(cb.pop(buffer, 12) == 12);
        REQUIRE(memcmp(buffer, "qwertyqwerty", 12) == 0);
    }

    cb.reset(5);
    REQUIRE(cb.push(nullptr, 3) == 0);
    REQUIRE(cb.push("qwerty", 6) == 5);
    REQUIRE(cb.isFull());
    REQUIRE(!cb.isEmpty());
    REQUIRE(cb.push(nullptr, 2) == 0);
    char buffer[100];
    INFO(cb.getCount());
    REQUIRE(cb.pop(buffer, 10) == 5);
    REQUIRE(memcmp(buffer, "qwerty", 5) == 0);

    cb.reset(15);

    for (int i = 0; i < 20; ++i)
    {
        REQUIRE(cb.push("qwert", 5) == 5);
        REQUIRE(cb.push('y'));

        char buffer[100];
        REQUIRE(cb.peek(buffer, 6) == 6);
        REQUIRE(memcmp(buffer, "qwerty", 6) == 0);
        REQUIRE(cb.drop(6) == 6);
    }
}

TEST_CASE("Resize function of the circular buffer", "[CircularBuffer]")
{
    {
        const CircularBuffer<int> cbOrig({1, 2, 3, 4, 5, 6, 7, 8, 9});
        const CircularBuffer<int> cbCopy(cbOrig);
        CircularBuffer<int> cb;
        cb = cbCopy;

        REQUIRE(cb.getCount() == 9);

        cb.drop(6);
        REQUIRE(cb.getCount() == 3);
        cb.push(8);
        cb.push(7);
        cb.push(6);
        cb.push(5);
        cb.push(4);
        cb.push(3);

        REQUIRE(cb.getCount() == 9);

        int buffer[100];
        REQUIRE(cb.peek(buffer, 100) == 9);
        const int expected[] = {7, 8, 9, 8, 7, 6, 5, 4, 3};
        REQUIRE(memcmp(buffer, expected, 9) == 0);

        REQUIRE(cb.getCount() == 9);
        cb.resize(7);
        REQUIRE(cb.getCount() == 7);
        REQUIRE(cb[0] == 7);
        REQUIRE(cb[1] == 8);
        REQUIRE(cb[2] == 9);
        REQUIRE(cb[3] == 8);

        REQUIRE(cb.peek(buffer, 100) == 7);
        REQUIRE(memcmp(buffer, expected, 7) == 0);

        INFO(cb[0]);
        INFO(cb[6]);

        cb.resize(6);

        REQUIRE(cb[0] == 7);
        REQUIRE(cb[1] == 8);
        REQUIRE(cb[2] == 9);
        REQUIRE(cb[3] == 8);

        REQUIRE(cb.peek(buffer, 100) == 6);
        REQUIRE(memcmp(buffer, expected, 6) == 0);
    }

    {
        CircularBuffer<int> cb({1,2,3,4,5,6,7,8,9});
        REQUIRE(cb.getCount() == 9);
        cb.drop(6);
        REQUIRE(cb.getCount() == 3);
        cb.push(8);
        cb.push(7);
        cb.push(6);
        cb.push(5);
        cb.push(4);
        cb.push(3);
        REQUIRE(cb.getCount() == 9);

        cb.resize(20);

        REQUIRE(cb.getCapacity() == 20);
        REQUIRE(cb.getCount() == 9);
        CHECK(cb[0] == 7);
        CHECK(cb[1] == 8);
        CHECK(cb[2] == 9);
        CHECK(cb[3] == 8);
        CHECK(cb[4] == 7);
        CHECK(cb[5] == 6);
        CHECK(cb[6] == 5);
        CHECK(cb[7] == 4);
        CHECK(cb[8] == 3);
    }
}

TEST_CASE("For-loop statement with begin and end", "[CircularBuffer]")
{
    const std::initializer_list<uint64_t> initlist = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    std::vector<uint64_t> v(initlist);
    std::vector<uint64_t> vTouched({6, 7, 8, 9, 1, 2, 3, 4, 5});
    const CircularBuffer<uint64_t> cb(initlist);

    int i = 0;
    for (const auto& val : cb) {
        REQUIRE(v[i++] == val);
    }

    {
        auto it = cb.end();
        --it;
        for (uint64_t i = 0; i < cb.getCount(); --it, ++i) {
            REQUIRE(v[cb.getCount() - i - 1] == *it);
        }
    }


    CircularBuffer<uint64_t> copy(cb);
    CircularBuffer<uint64_t>::iterator begin = copy.begin();
    CircularBuffer<uint64_t>::const_iterator cbegin = copy.cbegin();
    REQUIRE(begin == cbegin);
    CircularBuffer<uint64_t>::iterator end = copy.end();
    CircularBuffer<uint64_t>::const_iterator cend = copy.cend();
    REQUIRE(end == cend);
    {
        CircularBuffer<uint64_t>& cb = copy;
        REQUIRE(cb.drop(5) == 5);
        REQUIRE(cb.push(1));
        REQUIRE(cb.push(2));
        REQUIRE(cb.push(3));
        REQUIRE(cb.push(4));
        REQUIRE(cb.push(5));

        std::string strHint;
        for (uint64_t i = 0; i < cb.getCount(); ++i) {
            strHint += std::to_string(cb[i]) + ", ";
        }
        strHint += "\n";

        const uint64_t*const pData = cb.getRawBuffer();
        for (uint64_t i = 0; i < cb.getCount(); ++i) {
            strHint += std::to_string(pData[i]) + ", ";
        }
        strHint += "\n";

        for (const auto& val : cb) {
            strHint += std::to_string(val) + ", ";
        }
        INFO(strHint);

        uint64_t i = 0;
        for (const auto &val : cb) {
            REQUIRE(vTouched[i++] == val);
        }

        {
            auto it = cb.end();
            --it;
            for (uint64_t i = 0; i < cb.getCount(); --it, ++i) {
                REQUIRE(vTouched[cb.getCount() - i - 1] == *it);
            }
        }
    }
}
