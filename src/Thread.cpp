#include "Thread.h"
#include "Log.h"

#include <thread>

#ifdef WIN32
#include <mutex>
#include <map>
#include <windows.h>

namespace
{
    std::mutex g_mutexNames;
    std::map<DWORD, std::string> g_mapNames;

    const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)  
    typedef struct tagTHREADNAME_INFO
    {
        DWORD dwType; // Must be 0x1000.  
        LPCSTR szName; // Pointer to name (in user addr space).  
        DWORD dwThreadID; // Thread ID (-1=caller thread).  
        DWORD dwFlags; // Reserved for future use, must be zero.  
    } THREADNAME_INFO;
#pragma pack(pop)
    bool _SetThreadName(DWORD dwThreadID, const char*const threadName) {
        THREADNAME_INFO info;
        info.dwType = 0x1000;
        info.szName = threadName;
        info.dwThreadID = dwThreadID;
        info.dwFlags = 0;
#pragma warning(push)  
#pragma warning(disable: 6320 6322)  
        __try {
            RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
        }
        __except (EXCEPTION_EXECUTE_HANDLER) {
#ifdef _DEBUG
            return false;
#endif
        }
#pragma warning(pop)  
        return true;
    }
    bool SetThreadName(DWORD dwThreadID, const std::string& strName)
    {
        const bool bOk = _SetThreadName(dwThreadID, strName.c_str());
        if (bOk)
        {
            std::lock_guard<std::mutex> lock(g_mutexNames);
            g_mapNames.emplace(dwThreadID, strName);
            return true;
        }
        return false;
    }
    bool ClearThreadName(DWORD dwThreadID)
    {
        std::lock_guard<std::mutex> lock(g_mutexNames);
        const auto it = g_mapNames.find(dwThreadID);
        if (it != g_mapNames.end()) {
            g_mapNames.erase(it);
            return true;
        }
        return false;
    }
    bool GetThreadName(DWORD dwThreadID, std::string& strName)
    {
        std::lock_guard<std::mutex> lock(g_mutexNames);
        const auto it = g_mapNames.find(dwThreadID);
        if (it != g_mapNames.end()) {
            strName = it->second;
            return true;
        }
        return false;
    }
}
#endif

///////////////////////////////////////////////////////////////////////////////
Thread::Thread() : m_id(0), m_bCancelled(false), m_bFinished(false), m_bStartedSuccessfully(false)
{
}

///////////////////////////////////////////////////////////////////////////////
Thread::~Thread()
{
    {
        std::lock_guard<std::mutex> lock(m_mutexThread);
        if (m_apThread) {
            m_apThread->detach();
            m_apThread.release();
        }
    }
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::start()
{
    std::unique_lock<std::timed_mutex> lock(m_mutexRunning, std::try_to_lock);
    if (lock.owns_lock())
    {
        std::lock_guard<std::mutex> lock(m_mutexThread);
        if (m_apThread) {
            return false;
        }
        m_apThread.reset(new std::thread(&Thread::_run_, this));
#ifndef WIN32
        m_id = m_apThread->native_handle();
#else
        m_id = GetThreadId(m_apThread->native_handle());
#endif
        m_handle = reinterpret_cast<void*>(m_apThread->native_handle());
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::isCancelled() const
{
    return m_bCancelled;
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::isFinished() const
{
    return m_bFinished;
}

///////////////////////////////////////////////////////////////////////////////
void Thread::quit()
{
    m_bCancelled = true;
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::terminate()
{
#ifndef WIN32
    const pthread_t nId = m_id;
    return 0 == pthread_cancel(reinterpret_cast<pthread_t>(nId));
#else
    return 0 != TerminateThread(m_handle, -1);
#endif
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::isRunning() const
{
    std::unique_lock<std::timed_mutex> lock(m_mutexRunning, std::try_to_lock);
    return !lock.owns_lock();
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::wait(const unsigned int nTimeoutMs/* = ~0*/) const
{
    if (nTimeoutMs == ~(unsigned int)(0))
    {
        std::lock_guard<std::timed_mutex> lock(m_mutexRunning);
        return true;
    }

    const bool bLocked = m_mutexRunning.try_lock_for(std::chrono::milliseconds(nTimeoutMs));
    if (bLocked) { m_mutexRunning.unlock(); }
    return bLocked;
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::waitUntilRunning(const unsigned int nTimeoutMs/* = ~0*/) const
{
    UNUSED(nTimeoutMs);

    if (nTimeoutMs == ~(unsigned int)(0))
    {
        std::unique_lock<std::mutex> lock(m_mutexWaitUntilRunning);
        while (!isRunning() && !m_bFinished) {
            m_conditionVariableRunning.wait(lock);
        }
        return !m_bFinished;
    }
    else if (nTimeoutMs == 0)
    {
        return isRunning();
    }

    std::unique_lock<std::mutex> lock(m_mutexWaitUntilRunning);

    unsigned long long int nTimeLeftMs = nTimeoutMs;
    while (!isRunning() && !m_bFinished)
    {
        const auto tNow = std::chrono::steady_clock::now();
        const bool bTimeout = std::cv_status::timeout == m_conditionVariableRunning.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
        if (bTimeout)
        {
            return false;
        }
        const auto tThen = std::chrono::steady_clock::now();
        const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
        const unsigned long long int nElapsedMs = tElapsed.count();
        nTimeLeftMs -= nElapsedMs;
    }

    return !m_bFinished;
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::checkIfSuccessfullyStarted(const unsigned int nTimeoutMs/* = 0*/) const
{
    if (m_semStart.tryAcquire(1, nTimeoutMs))
    {
        return m_bStartedSuccessfully;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////////////
void Thread::releaseStart(const bool bSuccess)
{
    m_bStartedSuccessfully = bSuccess;
    m_semStart.release(1);
}

///////////////////////////////////////////////////////////////////////////////
void Thread::_run_()
{
    std::lock_guard<std::timed_mutex> lock(m_mutexRunning);
    {
        std::lock_guard<std::mutex> lock(m_mutexWaitUntilRunning);
        m_conditionVariableRunning.notify_all();
    }

    run();

    m_bFinished = true;

#ifdef WIN32
    ::ClearThreadName((DWORD)m_id);
#endif
}

///////////////////////////////////////////////////////////////////////////////
/*static */void Thread::ssleep(const unsigned int nSleepS)
{
    std::this_thread::sleep_for(std::chrono::seconds(nSleepS));
}

///////////////////////////////////////////////////////////////////////////////
/*static */void Thread::msleep(const unsigned int nSleepMs)
{
    std::this_thread::sleep_for(std::chrono::milliseconds(nSleepMs));
}

///////////////////////////////////////////////////////////////////////////////
/*static */void Thread::usleep(const unsigned int nSleepUs)
{
    std::this_thread::sleep_for(std::chrono::microseconds(nSleepUs));
}

///////////////////////////////////////////////////////////////////////////////
/*static */void Thread::nsleep(const unsigned int nSleepNs)
{
    std::this_thread::sleep_for(std::chrono::nanoseconds(nSleepNs));
}

///////////////////////////////////////////////////////////////////////////////
/*static */void Thread::yield()
{
    std::this_thread::yield();
}

///////////////////////////////////////////////////////////////////////////////
Thread::Id Thread::getId() const
{
    return m_id;
}

///////////////////////////////////////////////////////////////////////////////
void* Thread::getNativeHandle() const
{
    return m_handle;
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::setName(const std::string& strName)
{
    return setThreadName(m_id, strName);
}

///////////////////////////////////////////////////////////////////////////////
bool Thread::getName(std::string& strName) const
{
    return getThreadName(m_id, strName);
}

///////////////////////////////////////////////////////////////////////////////
std::string Thread::getName() const
{
    return getThreadName(m_id);
}

///////////////////////////////////////////////////////////////////////////////
/*static */bool Thread::setThreadName(const Thread::Id id, const std::string& strName)
{
#ifndef WIN32
    const pthread_t nId = reinterpret_cast<pthread_t>(id);
    return 0 == pthread_setname_np(nId, strName.substr(0, 15).c_str());
#else
    return ::SetThreadName((DWORD)id, strName.c_str());
#endif
}

///////////////////////////////////////////////////////////////////////////////
/*static */bool Thread::getThreadName(const Thread::Id id, std::string& strName)
{
#ifndef WIN32
    const pthread_t nId = reinterpret_cast<pthread_t>(id);
    char szName[16];
    const int nRetVal = pthread_getname_np(nId, szName, sizeof(szName));
    if (0 == nRetVal) {
        strName = std::string(szName);
        return true;
    }
    return false;
#else
    return ::GetThreadName((DWORD)id, strName);
#endif
}

///////////////////////////////////////////////////////////////////////////////
/*static */std::string Thread::getThreadName(const Thread::Id id)
{
    std::string strName;
    return getThreadName(id, strName), strName;
}

///////////////////////////////////////////////////////////////////////////////
/*static */ Thread::Id Thread::getCurrentThreadId()
{
#ifndef WIN32
    return reinterpret_cast<Thread::Id>(pthread_self());
#else
    return GetCurrentThreadId();
#endif
}

///////////////////////////////////////////////////////////////////////////////
/*static */bool Thread::setCurrentThreadName(const std::string& strName)
{
    return setThreadName(getCurrentThreadId(), strName);
}

///////////////////////////////////////////////////////////////////////////////
/*static */bool Thread::getCurrentThreadName(std::string& strName)
{
    return getThreadName(getCurrentThreadId(), strName);
}

///////////////////////////////////////////////////////////////////////////////
/*static */std::string Thread::getCurrentThreadName()
{
    return getThreadName(getCurrentThreadId());
}
