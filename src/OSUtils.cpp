#include "OSUtils.h"

#ifdef WIN32
#include <windows.h>
#else
#include <dirent.h>
#endif

#include <algorithm>

namespace
{
#ifdef WIN32
    const char c_cNativeSeparator = '\\';
    const char c_cNonNativeSeparator = '/';
#else
    const char c_cNativeSeparator = '/';
    const char c_cNonNativeSeparator = '\\';
#endif
}

namespace OSUtils
{

////////////////////////////////////////////////////////////////////////////////
std::list<std::string> getFilesInDirectory(const std::string &strDirPath)
{
    std::list<std::string> lstFiles;

#ifdef WIN32
    const std::string strSearchPath = strDirPath + "/*.*";
    WIN32_FIND_DATA fd;
    HANDLE hFind = ::FindFirstFile(strSearchPath.c_str(), &fd);
    if (hFind != INVALID_HANDLE_VALUE)
    {
        do {
            // read all (real) files in current folder
            // , delete '!' read other 2 default folder . and ..
            if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
                lstFiles.push_back(fd.cFileName);
            }
        } while (::FindNextFile(hFind, &fd));
        ::FindClose(hFind);
    }
#else
    DIR*const pDir = opendir(strDirPath.c_str());
    if (pDir)
    {
        dirent* pEnt = nullptr;
        while ((pEnt = readdir(pDir)))
        {
            const std::string strEnt = std::string(pEnt->d_name);
            lstFiles.push_back(strEnt);
        }
        closedir(pDir);
    }
#endif
    return lstFiles;
}

////////////////////////////////////////////////////////////////////////////////
std::string cleanPath(const std::string &strPath)
{
    std::string strCleanPath = strPath;

    // Convert to native separator first.
    std::replace(strCleanPath.begin(), strCleanPath.end(), c_cNonNativeSeparator, c_cNativeSeparator);

    // Then remove duplicates.
    struct both_slashes {
        bool operator()(char a, char b) const {
            return a == c_cNativeSeparator && b == c_cNativeSeparator;
        }
    };
    strCleanPath.erase(std::unique(strCleanPath.begin(), strCleanPath.end(), both_slashes()), strCleanPath.end());

    return strCleanPath;
}

#ifdef WIN32
namespace
{
    bool isValidUnit(const char c)
    {
        return ('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z');
    }
}
#endif

////////////////////////////////////////////////////////////////////////////////
bool isAbsolutePath(const std::string &strPath)
{
#ifdef WIN32
    // <unit>:<slash>...
    return strPath.length() >= 2 && isValidUnit(strPath[0]) && strPath[1] == ':';
#else
    // <slash>...
    return strPath.length() >= 1 && strPath[0] == '/';
#endif
}

////////////////////////////////////////////////////////////////////////////////
char getNativeSeparator()
{
    return c_cNativeSeparator;
}

}
