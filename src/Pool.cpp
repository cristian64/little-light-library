#include "Pool.h"

#include <cassert>

///////////////////////////////////////////////////////////////////////////////
Pool::Pool() : m_bCancelled(false)
{
}

///////////////////////////////////////////////////////////////////////////////
Pool::~Pool()
{
    cancel();

#ifndef NDEBUG
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        assert(m_mapOutside.size() == 0);
        assert(m_mapInside.size() == 0);
    }
#endif
}

///////////////////////////////////////////////////////////////////////////////
std::string Pool::getPoolName() const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_strPoolName;
}

///////////////////////////////////////////////////////////////////////////////
void Pool::setPoolName(const std::string& strPoolName)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_strPoolName = strPoolName;
}

///////////////////////////////////////////////////////////////////////////////
bool Pool::cancel(const unsigned int nTimeoutMs/* = 0*/)
{
    bool bOk = false;

    std::unique_lock<std::mutex> lock(m_mutex);

    m_bCancelled = true;

    if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (m_mapOutside.size())
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                goto freeInternal;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (m_mapOutside.size())
    {
        goto freeInternal;
    }

    bOk = true;

freeInternal:
    for (const auto& it : m_mapInside) {
        delete it;
    }
    m_mapInside.clear();
    for (const auto& it : m_mapOutside) {
        it->setPool(nullptr);
    }
    m_mapOutside.clear();

    return bOk;
}

///////////////////////////////////////////////////////////////////////////////
void Pool::addContent(PoolContent*const pContent)
{
    if (!pContent) { return; }

    {
        std::lock_guard <std::mutex> lock(m_mutex);
        if (m_bCancelled) { delete pContent; return; }
        pContent->setPool(this);
        assert(m_mapInside.find(pContent) == m_mapInside.end());
        assert(m_mapOutside.find(pContent) == m_mapOutside.end());
        m_mapInside.insert(pContent);
    }
    m_conditionVariable.notify_all();
}

///////////////////////////////////////////////////////////////////////////////
void Pool::returnContent(PoolContent*const pContent)
{
    {
        std::lock_guard <std::mutex> lock(m_mutex);
        if (!m_bCancelled)
        {
            assert(m_mapInside.find(pContent) == m_mapInside.end());
            assert(m_mapOutside.find(pContent) != m_mapOutside.end());
            m_mapInside.insert(pContent);
            m_mapOutside.erase(m_mapOutside.find(pContent));
        }
        else
        {
            const auto it = m_mapOutside.find(pContent);
            if (it != m_mapOutside.end()) {
                m_mapOutside.erase(it);
            }
            delete pContent;
        }
    }
    m_conditionVariable.notify_all();
}

///////////////////////////////////////////////////////////////////////////////
bool Pool::getContent(PoolContentContainer& container, const unsigned int nTimeoutMs/* = 0*/)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (m_mapInside.size() == 0 && !m_bCancelled)
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                return false;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (m_mapInside.size() == 0 || m_bCancelled)
    {
        return false;
    }

    if (m_bCancelled)
    {
        return false;
    }

    const auto it = m_mapInside.begin();
    container = *it;
    m_mapOutside.insert(*it);
    m_mapInside.erase(it);

    return true;
}

///////////////////////////////////////////////////////////////////////////////
void Pool::getPoolStatus(size_t& nInside, size_t& nOutside) const
{
    std::lock_guard<std::mutex> lock(m_mutex);
    nInside = m_mapInside.size();
    nOutside = m_mapOutside.size();
}

///////////////////////////////////////////////////////////////////////////////
void Pool::onDereferenced()
{
    delete this;
}
