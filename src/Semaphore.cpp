#include "Semaphore.h"

///////////////////////////////////////////////////////////////////////////////
Semaphore::Semaphore(const unsigned int nCount/* = 0*/) : m_nCount(nCount)
{
}

///////////////////////////////////////////////////////////////////////////////
void Semaphore::release(const unsigned int nCount/* = 1*/)
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_nCount += nCount;
    }
    m_conditionVariable.notify_one();
}

///////////////////////////////////////////////////////////////////////////////
void Semaphore::acquire(const unsigned int nCount/* = 1*/)
{
    std::unique_lock<std::mutex> lock(m_mutex);
    while (m_nCount < nCount)
    {
        m_conditionVariable.wait(lock);
    }
    m_nCount -= nCount;
}

///////////////////////////////////////////////////////////////////////////////
bool Semaphore::tryAcquire(const unsigned int nCount/* = 1*/, const unsigned int nTimeoutMs/* = 0*/)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (m_nCount < nCount)
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                return false;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (m_nCount < nCount)
    {
        return false;
    }

    m_nCount -= nCount;
    return true;
}
