#include "Random.h"

///////////////////////////////////////////////////////////////////////////////
Random::Random(const int nSeed/* = 0*/) : m_nSeed(nSeed)
{
}

///////////////////////////////////////////////////////////////////////////////
void Random::reset(const int nSeed/* = 0*/)
{
    m_nSeed = nSeed;
}

///////////////////////////////////////////////////////////////////////////////
int Random::rand()
{
    unsigned int nNext = m_nSeed;
    int nResult;

    nNext *= 1103515245;
    nNext += 12345;
    nResult = (unsigned int) (nNext / 65536) % 2048;

    nNext *= 1103515245;
    nNext += 12345;
    nResult <<= 10;
    nResult ^= (unsigned int) (nNext / 65536) % 1024;

    nNext *= 1103515245;
    nNext += 12345;
    nResult <<= 10;
    nResult ^= (unsigned int) (nNext / 65536) % 1024;

    m_nSeed = nNext;

    return nResult;
}
