#include "WaitableObject.h"

#include <cassert>

///////////////////////////////////////////////////////////////////////////////
WaitableObject::WaitableObject() : m_nCount(0)
{
}

///////////////////////////////////////////////////////////////////////////////
WaitableObject::~WaitableObject()
{
}

///////////////////////////////////////////////////////////////////////////////
void WaitableObject::incCount(const unsigned int nCount/* = 1*/)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_nCount += nCount;
}

///////////////////////////////////////////////////////////////////////////////
void WaitableObject::decCount(const unsigned int nCount/* = 1*/)
{
    std::lock_guard<std::mutex> lock(m_mutex);
    assert(m_nCount >= nCount);
    m_nCount -= nCount;
    if (m_nCount == 0)
    {
        m_conditionVariable.notify_all();
    }
}

///////////////////////////////////////////////////////////////////////////////
bool WaitableObject::waitForObject(const unsigned int nTimeoutMs/* = ~0*/) const
{
    std::unique_lock<std::mutex> lock(m_mutex);

    if (nTimeoutMs == ~(unsigned int)(0))
    {
        while (m_nCount)
        {
            m_conditionVariable.wait(lock);
        }
    }
    else if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (m_nCount)
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                return false;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (m_nCount)
    {
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
void WaitableObject::flush()
{
    std::lock_guard<std::mutex> lock(m_mutex);
    m_nCount = 0;
    m_conditionVariable.notify_all();
}
