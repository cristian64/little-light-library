#include "ReadWriteMutex.h"

///////////////////////////////////////////////////////////////////////////////
ReadWriteMutex::ReadWriteMutex() : m_nReaders(0), m_nWriters(0)
{
}

///////////////////////////////////////////////////////////////////////////////
void ReadWriteMutex::lockForRead()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    while (m_nWriters)
    {
        m_conditionVariable.wait(lock);
    }
    ++m_nReaders;
}

///////////////////////////////////////////////////////////////////////////////
void ReadWriteMutex::lockForWrite()
{
    std::unique_lock<std::mutex> lock(m_mutex);
    while (m_nWriters || m_nReaders)
    {
        m_conditionVariable.wait(lock);
    }
    ++m_nWriters;
}

///////////////////////////////////////////////////////////////////////////////
void ReadWriteMutex::unlockForRead()
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_nReaders) { --m_nReaders; }
    }
    m_conditionVariable.notify_one();
}

///////////////////////////////////////////////////////////////////////////////
void ReadWriteMutex::unlockForWrite()
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        if (m_nWriters) { --m_nWriters; }
    }
    m_conditionVariable.notify_one();
}

///////////////////////////////////////////////////////////////////////////////
bool ReadWriteMutex::tryLockForRead(const unsigned int nTimeoutMs/* = 0*/)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (m_nWriters)
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                return false;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (m_nWriters)
    {
        return false;
    }

    ++m_nReaders;
    return true;
}

///////////////////////////////////////////////////////////////////////////////
bool ReadWriteMutex::tryLockForWrite(const unsigned int nTimeoutMs/* = 0*/)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (m_nWriters || m_nReaders)
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                return false;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (m_nWriters || m_nReaders)
    {
        return false;
    }

    ++m_nWriters;
    return true;
}

///////////////////////////////////////////////////////////////////////////////
ReadMutexLocker::ReadMutexLocker(ReadWriteMutex& mutex) : m_mutex(mutex)
{
    m_mutex.lockForRead();
}

///////////////////////////////////////////////////////////////////////////////
ReadMutexLocker::~ReadMutexLocker()
{
    m_mutex.unlockForRead();
}

///////////////////////////////////////////////////////////////////////////////
WriteMutexLocker::WriteMutexLocker(ReadWriteMutex& mutex) : m_mutex(mutex)
{
    m_mutex.lockForWrite();
}

///////////////////////////////////////////////////////////////////////////////
WriteMutexLocker::~WriteMutexLocker()
{
    m_mutex.unlockForWrite();
}
