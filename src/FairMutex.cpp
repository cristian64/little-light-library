#include "FairMutex.h"

#include <iostream>

///////////////////////////////////////////////////////////////////////////////
FairMutex::FairMutex() : m_nTicketGenerator(0), m_nNextTicket(0)
{
}

///////////////////////////////////////////////////////////////////////////////
void FairMutex::lock()
{
    std::unique_lock<std::mutex> lock(m_mutex);

    int nTicket = m_nTicketGenerator++;
    while (nTicket != m_nNextTicket)
    {
        m_conditionVariable.wait(lock);
    }
}

///////////////////////////////////////////////////////////////////////////////
void FairMutex::unlock()
{
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        do
        {
            const auto it = setTimedoutTickets.find(++m_nNextTicket);
            if (it == setTimedoutTickets.cend()) { break; }
            setTimedoutTickets.erase(it);
        }
        while (true);
    }
    m_conditionVariable.notify_all();
}

///////////////////////////////////////////////////////////////////////////////
bool FairMutex::tryLock(const unsigned int nTimeoutMs/* = 0*/)
{
    std::unique_lock<std::mutex> lock(m_mutex);

    int nTicket = m_nTicketGenerator++;

    if (nTimeoutMs)
    {
        unsigned long long int nTimeLeftMs = nTimeoutMs;
        while (nTicket != m_nNextTicket)
        {
            const auto tNow = std::chrono::steady_clock::now();
            const bool bTimeout = std::cv_status::timeout == m_conditionVariable.wait_for(lock, std::chrono::milliseconds(nTimeLeftMs));
            if (bTimeout)
            {
                setTimedoutTickets.insert(nTicket);
                return false;
            }
            const auto tThen = std::chrono::steady_clock::now();
            const auto tElapsed = std::chrono::duration_cast<std::chrono::milliseconds>(tThen - tNow);
            const unsigned long long int nElapsedMs = tElapsed.count();
            nTimeLeftMs -= nElapsedMs;
        }
    }
    else if (nTicket != m_nNextTicket)
    {
        setTimedoutTickets.insert(nTicket);
        return false;
    }

    return true;
}

///////////////////////////////////////////////////////////////////////////////
FairMutexLocker::FairMutexLocker(FairMutex& mutex) : m_mutex(mutex)
{
    m_mutex.lock();
}

///////////////////////////////////////////////////////////////////////////////
FairMutexLocker::~FairMutexLocker()
{
    m_mutex.unlock();
}