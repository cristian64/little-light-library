#include "Log.h"
#include "Thread.h"

#include <iostream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <algorithm>
#include <cstring>

namespace
{
    std::string getDateTimeAsString()
    {
        // Get the current time in a good format.
        const auto tpNow = std::chrono::system_clock::now();
        const auto tpNowMs = std::chrono::time_point_cast<std::chrono::milliseconds>(tpNow);
        const auto msEpoch = tpNowMs.time_since_epoch();
        const auto nMsEpoch = msEpoch.count();
        const long int nMs = nMsEpoch % 1000;

        char szDateTime[24];
        const std::time_t ttNow = std::chrono::system_clock::to_time_t(tpNow);
#ifdef WIN32
        tm timeinfo;
        localtime_s(&timeinfo, &ttNow);
        strftime(szDateTime, 21, "%Y-%m-%d %H:%M:%S.", &timeinfo);
#else
        strftime(szDateTime, 21, "%Y-%m-%d %H:%M:%S.", std::localtime(&ttNow));
#endif
        snprintf(&szDateTime[20], 4, "%03li", nMs);


        return std::string(szDateTime);
    }
}

std::mutex Log::s_mutex;

///////////////////////////////////////////////////////////////////////////////
Log::Log() : m_nLevel(0)
{
}

///////////////////////////////////////////////////////////////////////////////
Log::Log(const int nLevel) : m_nLevel(nLevel)
{
}

///////////////////////////////////////////////////////////////////////////////
Log::Log(const int nLevel, const std::string& strSystem) :
    m_nLevel(nLevel),
    m_strSystem(strSystem)
{
}

///////////////////////////////////////////////////////////////////////////////
Log::Log(const std::string& strSystem) :
    m_nLevel(0),
    m_strSystem(strSystem)
{
}

///////////////////////////////////////////////////////////////////////////////
Log::~Log()
{
    // Get timestamp formatted as "yyyy-mm-dd hh:mm:ss.zzz".
    const std::string strDateTime = getDateTimeAsString();

    // Get the lock and then print to avoid interleaved characters.
    std::unique_lock<std::mutex> lock(s_mutex);
    if (m_nLevel)
    {
        std::cout << "(" << m_nLevel << ") " << strDateTime << " ";
    }
    else
    {
        std::cout << "    " << strDateTime << " ";
    }
    std::cout << std::left << std::setw(32) << m_strSystem.substr(0, std::min<size_t>(32, m_strSystem.length()));
    std::cout << std::left << std::setw(32) << Thread::getCurrentThreadName();
    for (const std::string& str : m_lstParts)
    {
        std::cout << " " << str;
    }
    std::cout << std::endl << std::flush;
}

///////////////////////////////////////////////////////////////////////////////
Log& Log::operator<<(const bool b)
{
    m_lstParts.push_back(b ? "true" : "false");
    return *this;
}

///////////////////////////////////////////////////////////////////////////////
Log& Log::operator<<(const char c)
{
    m_lstParts.push_back(std::string(1, c));
    return *this;
}

///////////////////////////////////////////////////////////////////////////////
Log& Log::operator<<(const std::string& str)
{
    m_lstParts.push_back(str);
    return *this;
}

///////////////////////////////////////////////////////////////////////////////
Log& Log::operator<<(const char*const str)
{
    m_lstParts.push_back(std::string(str));
    return *this;
}
