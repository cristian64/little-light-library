# README #

Light Little Library is a compound of classes that provide threading, locks, semaphores and synchronization mechanisms (and maybe more in the future). It is written in C++11.
